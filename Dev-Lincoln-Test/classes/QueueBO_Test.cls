@isTest
private class QueueBO_Test {
	
	@isTest static void test_method_one() {
		Account accountInsertMagento = SObjectInstanceToClassCoverage.accountInstance( null );
		insert accountInsertMagento;

		QueueBO.getInstance().sObjectQueue( accountInsertMagento.Id, QueueEventNames.INSERT_ACCOUNT_SF_TO_MAGENTO.name(), 'XML', true );
	}
	
	@isTest static void test_method_two() {
		QueueBO.getInstance().insertQueue( QueueEventNames.INSERT_ACCOUNT_SF_TO_MAGENTO.name(), 'XML', true );
	}

	@isTest static void test_method_three() {
		String queueId = QueueBO.getInstance().insertQueue( QueueEventNames.INSERT_ACCOUNT_SF_TO_MAGENTO.name(), 'XML', true );

		QueueBO.getInstance().updateQueue( queueId, '' );
	}

	@isTest static void test_method_four() {
		String queueId = QueueBO.getInstance().insertQueue( QueueEventNames.INSERT_ACCOUNT_SF_TO_MAGENTO.name(), 'XML', true );

		QueueBO.getInstance().updateQueue( queueId, '', '' );
	}

	@isTest static void test_method_five() {
		QueueBO.getInstance().reprocessQueue();
	}

	@isTest static void test_method_six() {
		QueueDAO.getInstance().getQueueById('ID');
		QueueDAO.getInstance().getQueueByStatus('CREATED');
	}
	
}