@isTest(SeeAllData=true)
private class AccountBO_Test {
	
	@isTest static void test_method_one() {

		Account accountEmpiricusInsertMagento = SObjectInstanceToClassCoverage.accountEmpiricusInstance( null );
		insert accountEmpiricusInsertMagento;

		Account accountEmpiricusInsertJovili = SObjectInstanceToClassCoverage.accountInstanceJoliviBR( null );
		insert accountEmpiricusInsertJovili; 

		List<String> emailsToTest = new List<String>();

		AccountBO.getInstance().upsertAccountWStore( '{"objects":[{"date_birthday":"17/05/1979","email":"rogerio@beecloud.com.br","orders":[],"cpf_cnpj":"275.325.968-26","phone":"11-99610-4536","id":265184,"mobile":"11-99610-4536","subscriptions":[],"name":"Rogério Luiz Batista De Oliveira","subscriptions_total":0,"total_spent":0}],"meta":{"next":false,"page":1,"limit":30,"previous":false,"total_count":1}}' );
		AccountInboundBO.updateAccountWStore( '{"objects":[{"date_birthday":"17/05/1979","email":"rogerio@beecloud.com.br","orders":[],"cpf_cnpj":"275.325.968-26","phone":"11-99610-4536","id":265184,"mobile":"11-99610-4536","subscriptions":[],"name":"Rogério Luiz Batista De Oliveira","subscriptions_total":0,"total_spent":0}],"meta":{"next":false,"page":1,"limit":30,"previous":false,"total_count":1}}');
		AccountDAO.getInstance().getAccountsByEmails( emailsToTest );
		ContactDAO.getInstance().getContactsByEmails( emailsToTest );

		AccountBO.getInstance().updateAccountWStore( accountEmpiricusInsertJovili.Id, '' ); 

		AccountStore.updateAccountWStore( accountEmpiricusInsertJovili.Id, '' );

		/*Case thisCase = SObjectInstanceToClassCoverage.caseInstance();
    	insert thisCase;*/
	}
}