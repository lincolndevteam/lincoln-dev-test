/**
*	@author Diego Moreira
*	@class Classe de metodos utilitarios
*/
public class Util {
	/**
	* 	Retorna uma String a partir de um Object. 
 	* 	Se o objeto for igual a NULL, retorna uma String vazia, caso contrário, faz a conversão e retorna uma String sem os espaços iniciais e finais(trim()). 
 	* 	@param obj Um Object qualquer.
 	* 	@return O objeto convertido em String.
 	*/
    public static String getString( Object obj ) { 
        return ( obj == null ? '' : String.valueOf( obj ) );
    }

    /**
    *  Retorna o próprio Decimal recebido se ele for diferente de NULL.
    *  Se for igual a NULL retorna ZERO.
    *  @param valor Um Decimal qualquer.
    *  @return O próprio Decimal recebido ou ZERO.
   */
   public static Decimal getDecimal( String valor ) {
       return ( valor == null || valor == '' ? 0 : Decimal.valueOf( valor ) );
   }

    /**
    *   Retorna uma data de um objeto
    *   @param result Valor a ser convertido
    */
    public static Date getObjectToDate( Object result ) {
        Date dt = null;

        if (result != null && Util.getString( result ) != ''){
            List<String> dtResult = Util.getString( result ).split( '/' );
            if(dtResult[0] == null || dtResult[0] == '00')
                return dt;
            dt = Date.newInstance(Integer.valueOf( dtResult[2] ), Integer.valueOf( dtResult[1] ), Integer.valueOf( dtResult[0] ) );        
        }
        
        return dt;

    }


   /**
    *   Retorna uma data de um objeto
    *   @param result Valor a ser convertido
    */
    public static Date getObjectToDate2( Object result ) {
        Date dt = null;

        if (result != null && Util.getString( result ) != ''){
            List<String> dtResult = Util.getString( result ).split( 'T' );
            List<String> sDate = dtResult[0].split('-');
            dt = Date.newInstance(Integer.valueOf( sDate[0] ), Integer.valueOf( sDate[1] ), Integer.valueOf( sDate[2] ) );        
        }
        
        return dt;

    }

    /**
    *   Valida se é um email valido
    *   @param email Email para validação
    */
    public static Boolean validarEmail( String email ) {
        if (String.isBlank(email))
            return false;

        if (email.indexOf('..') > -1)
            return false;

        if(Pattern.matches('[a-zA-Z0-9\\.\\!\\#\\$\\%\\&\\*\\/\\=\\?\\^\\_\\+\\-\\`\\{\\|\\}\\~\'._%+-]+@[a-zA-Z0-9\\-.-]+\\.[a-zA-Z]+', email))
            return true;
        else
            return false;
    }

    /**
    *
    *
    */
    public static String updateYahooMail( String email ) {
        Integer i = email.indexOf('@');
        email = email.substring( 0, i );
        email = email.replace( '.yahoo', '@yahoo' );

        return email;
    }

   /**
    *   Retorna uma data de um objeto
    *   @param result Valor a ser convertido
    */
    public static DateTime getObjectToDateTime( Object result ) {
        DateTime dt = null;

        if (result != null && Util.getString( result ) != ''){
            List<String> dtResult = Util.getString( result ).split( ' ' );
            List<String> sDate = dtResult[0].split('/');
            List<String> sTime = dtResult[1].split(':');
            dt = DateTime.newInstance(Integer.valueOf( sDate[2] ), Integer.valueOf( sDate[1] ), Integer.valueOf( sDate[0] ), Integer.valueOf( sTime[0] ), Integer.valueOf( sTime[1] ), Integer.valueOf( sTime[2] ) );        
        }
        
        return dt;

    }
    /**
     * Retorna um boolean
     * Se for igual a NULL retorna FALSE.
     * @param valor de uma objeto qualquer
     * @return O próprio Boolean recebido ou false.
     */
    public static Boolean getBoolean( Object obj ) {
        return ( obj == null ? false : Boolean.valueOf( obj ) );
    }

    /**
    *   Leitor de XML
    *   @param node Nó principal do XML
    *   @param field Qual a informação que deseja retornar do XML
    */
    public static String readXMLTag( DOM.XMLNode node, String field ) {
        String result = '\n';    
        if( node.getNodeType() == DOM.XMLNodeType.ELEMENT ) {            
            if(node.getName().trim() == field) {
                result += node.getText().trim();
            }
            
            for(DOM.XMLNode child : node.getChildElements()) {
                result += readXMLTag( child, field );
            }
            return result;
        }
        return '';
    }

    /**
    *
    *
    */
    public static String readXMLError( DOM.XMLNode node ) {
        String faultCode = readXMLTag( node, 'faultcode' ).trim();
        String faultString = readXMLTag( node, 'faultstring' ).trim();
  
        String error = faultCode != '' ? faultCode + ' - ' + faultString : ''; 
        return error;
    }

    /**
    *   Retorna modelo dados apos integração
    *   @param dmlResult resultado de inserção na base de dados
    *   @param mapSObject Mapa com os dados do registro criado
    */
    public static String jsonResponse( Database.SaveResult[] dmlResult, Map<String, String> mapCodigoMagento ) {
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName( 'result' );
            gen.writeStartArray();          
            for( Database.SaveResult result : dmlResult ) {
                gen.writeStartObject();
                gen.writeStringField( 'sfid', Util.getString( result.getId() ) ); 
                gen.writeStringField( 'codigomagento', Util.getString( mapCodigoMagento.get( result.getId() ) ) ); 
                gen.writeStringField( 'status', result.isSuccess() ? 'OK' : 'NOK' ); 
                gen.writeFieldName( 'error' );
                    gen.writeStartArray();          
                    for( Database.Error errorResult : result.getErrors() ) {
                        gen.writeStartObject();
                        gen.writeStringField( 'code', Util.getString( errorResult.getStatusCode() ) );
                        gen.writeStringField( 'message', Util.getString( errorResult.getMessage() ) );
                        gen.writeStringField( 'fields', Util.getString( errorResult.getFields() ) );
                        gen.writeEndObject(); 
                    }
                    gen.writeEndArray(); 
                gen.writeEndObject();  
            }          
            gen.writeEndArray();    
        gen.writeEndObject(); 

        return gen.getAsString();
    }

    /**
    *    Retorna modelo dados apos integração
    *    @param status de retorno do serviço
    *    @param mensagem de retorno do serviço
    */
    public static String jsonGenericResponse( String mensagem ) {
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName( 'result' );
            gen.writeStartArray();          
                gen.writeStartObject();
                gen.writeStringField( 'sfid', '' ); 
                gen.writeStringField( 'codigomagento', '' ); 
                gen.writeStringField( 'status', 'NOK' ); 
                gen.writeFieldName( 'error' );
                    gen.writeStartArray();          
                        gen.writeStartObject();
                        gen.writeStringField( 'code', 'APEX_ERROR' );
                        gen.writeStringField( 'message', mensagem );
                        gen.writeStringField( 'fields', '' );
                        gen.writeEndObject(); 
                    gen.writeEndArray(); 
                gen.writeEndObject();      
            gen.writeEndArray();    
        gen.writeEndObject();

        return gen.getAsString();
    }

     /**
     * Retorna o primeiro nome de uma string
     * @param name nome que deseja separar
     */ 
    public static String getFirstName( String name ) {
        Integer i = name.indexOf(' ');
        return i >= 0 ? name.substring( 0, i ) : name;
    }
    
    /**
     * Retorna o Sobrenome de uma string
     * @param name nome que deseja separar
     */ 
    public static String getLastName( String name ) {
        String result = name.replace( getFirstName( name ), '' ).trim();
        return result.equals( '' ) ? 'N/A' : result;
    }

}