/**
*   @author Diego Moreira
*   @class Classe de negocio da integração de contas
*/
public class AccountOutboundBO implements IQueueProcessing {
	/** 
    *   Processamento da fila 
    *	@param queueId Id da fila de processamento 
    *	@param recordId Id de registro	
    *   @param eventName nome do evento de processamento
    *   @param payload JSON com o item da fila para processamento
    */     
    public static void execute( String queueId, String recordId, String eventName, String payload ) {
    	syncToServer( queueId, recordId, eventName, payload );
    }

    @future( Callout=true )
    @TestVisible private static void syncToServer( String queueId, String recordId, String eventName, String payload ) {
        try {
            String loginResult; 
            if( Test.isRunningTest() ) loginResult = MagentoOutboundUtil.getXMLLoginResponse_Test();
            else loginResult = MagentoOutboundIS.getInstance().doPost( MagentoOutboundUtil.getXMLLogin() );
        
            DOM.Document doc = new DOM.Document();
            doc.load( loginResult );                
            DOM.XMLNode xmlNode = doc.getRootElement();

            String token = Util.readXMLTag( xmlNode, 'loginReturn').trim();
            String serviceError = Util.readXMLError( xmlNode );

            if( String.isNotBlank( token ) ) {
                payload = String.format( payload, new String[]{ token } );

                if( eventName.equals( QueueEventNames.INSERT_ACCOUNT_SF_TO_MAGENTO.name() ) ) {
                    createCustomerMagento( queueId, recordId, payload );             
                } else if( eventName.equals( QueueEventNames.UPDATE_ACCOUNT_SF_TO_MAGENTO.name() ) ) {
                    updateCustomerMagento( queueId, payload );    
                } 
            } else {
                QueueBO.getInstance().updateQueue( queueId, loginResult, serviceError );
            }                                                         
        } catch( CalloutException ex ) {
            QueueBO.getInstance().updateQueue( queueId, eventName + ' / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
        } catch( Exception ex ) {
            QueueBO.getInstance().updateQueue( queueId, eventName + ' / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
        }  
    }

    /**
    *   Envia os dados de criação de cliente para o Magento
    *   @param queueId Id da fila de processamento
    *   @param recordId Id do registro que gerou a transação
    *   @param payload XML com os dados de transação
    */
    private static void createCustomerMagento( String queueId, String recordId, String payload ) {
        String response = MagentoOutboundIS.getInstance().doPost( payload );

        DOM.Document doc = new DOM.Document();
        doc.load( response );                
        DOM.XMLNode xmlNode = doc.getRootElement();

        String magentoCode = Util.readXMLTag( xmlNode, 'result' );
        String serviceError = Util.readXMLError( xmlNode );

        if( String.isNotBlank( magentoCode ) ) {
            ProcessControl.inFutureContext = true;
            update new Account( Id = recordId, CodigoMagento__c = magentoCode );
        }

        QueueBO.getInstance().updateQueue( queueId, response, serviceError );        
    }

    /**
    *   Envia os dados de atualização de cliente para o Magento
    *   @param queueId Id da fila de processamento
    *   @param payload XML com os dados de transação
    */
    private static void updateCustomerMagento( String queueId, String payload ) {
        String response = MagentoOutboundIS.getInstance().doPost( payload );

        DOM.Document doc = new DOM.Document();
        doc.load( response );                
        DOM.XMLNode xmlNode = doc.getRootElement();

        String serviceError = Util.readXMLError( xmlNode );        
        QueueBO.getInstance().updateQueue( queueId, response, serviceError );
    }

    /**
	*	Cria JSON com os dados do objeto
	*	@param account objeto de contas para integração
    */
    public static String createRequest( Account account ) {
        String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
        String xsi = 'http://www.w3.org/2001/XMLSchema-instance';
        String xsd = 'http://www.w3.org/2001/XMLSchema';
        String urn = 'urn:Magento';

        DOM.Document doc = new DOM.Document();
        dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');
        envelope.setNamespace( 'xsi', xsi );
        envelope.setNamespace( 'xsd', xsd );
        envelope.setNamespace( 'urn', urn );

        dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
        dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
        dom.XmlNode customer = body.addChildElement('customerCustomerCreate', urn, null);
        dom.XmlNode sessionId = customer.addChildElement( 'sessionId', null, null);
        sessionId.addTextNode( '{0}' );
        dom.XmlNode data = customer.addChildElement('customerData', null, null);
        dom.XmlNode customer_id = data.addChildElement( 'customer_id', null, null);
        customer_id.addTextNode( Util.getString( account.CodigoMagento__c ) );
        dom.XmlNode email = data.addChildElement( 'email', null, null);
        email.addTextNode( Util.getString( account.Email__c ) );
        dom.XmlNode firstname = data.addChildElement( 'firstname', null, null);
        firstname.addTextNode( Util.getString( account.Firstname ) );
        dom.XmlNode lastname = data.addChildElement( 'lastname', null, null);
        lastname.addTextNode( Util.getString( account.LastName ) );
        dom.XmlNode middlename = data.addChildElement( 'middlename', null, null);
        middlename.addTextNode( '' );
        dom.XmlNode password = data.addChildElement( 'password', null, null);
        password.addTextNode( '' );
        dom.XmlNode website_id = data.addChildElement( 'website_id', null, null);
        website_id.addTextNode( '' );
        dom.XmlNode store_id = data.addChildElement( 'store_id', null, null);
        store_id.addTextNode( '' );
        dom.XmlNode group_id = data.addChildElement( 'group_id', null, null);
        group_id.addTextNode( '' );
        dom.XmlNode prefix = data.addChildElement( 'prefix', null, null);
        prefix.addTextNode( '' );
        dom.XmlNode suffix = data.addChildElement( 'suffix', null, null);
        suffix.addTextNode( '' );
        dom.XmlNode dob = data.addChildElement( 'dob', null, null);
        dob.addTextNode( '' );
        dom.XmlNode taxvat = data.addChildElement( 'taxvat', null, null);
        taxvat.addTextNode( Util.getString( account.CPF__c ) );
        dom.XmlNode gender = data.addChildElement( 'gender', null, null);
        gender.addTextNode( '' );
        dom.XmlNode sfid = data.addChildElement( 'sfid', null, null );
        sfid.addTextNode( account.Id );

        return doc.toXmlString();
    }

    /**
    *   Cria JSON com os dados do objeto
    *   @param account objeto de contas para integração
    */
    public static String updateRequest( Account account ) {
        String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
        String xsi = 'http://www.w3.org/2001/XMLSchema-instance';
        String xsd = 'http://www.w3.org/2001/XMLSchema';
        String urn = 'urn:Magento';

        DOM.Document doc = new DOM.Document();
        dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');
        envelope.setNamespace( 'xsi', xsi );
        envelope.setNamespace( 'xsd', xsd );
        envelope.setNamespace( 'urn', urn );

        dom.XmlNode header = envelope.addChildElement( 'Header', soapNS, null );
        dom.XmlNode body = envelope.addChildElement( 'Body', soapNS, null );
        dom.XmlNode customer = body.addChildElement( 'customerCustomerUpdate', urn, null );
        dom.XmlNode sessionId = customer.addChildElement( 'sessionId', null, null );
        sessionId.addTextNode( '{0}' );
        dom.XmlNode customerId = customer.addChildElement( 'customerId', null, null );
        customerId.addTextNode( Util.getString( account.CodigoMagento__c ) );
        dom.XmlNode data = customer.addChildElement( 'customerData', null, null );
        dom.XmlNode customer_id = data.addChildElement( 'customer_id', null, null );
        customer_id.addTextNode( Util.getString( account.CodigoMagento__c ) );
        dom.XmlNode email = data.addChildElement( 'email', null, null );
        email.addTextNode( Util.getString( account.Email__c ) );
        dom.XmlNode firstname = data.addChildElement( 'firstname', null, null );
        firstname.addTextNode( Util.getString( account.Firstname ) );
        dom.XmlNode lastname = data.addChildElement( 'lastname', null, null );
        lastname.addTextNode( Util.getString( account.LastName ) );
        dom.XmlNode middlename = data.addChildElement( 'middlename', null, null );
        middlename.addTextNode( '' );
        dom.XmlNode password = data.addChildElement( 'password', null, null );
        password.addTextNode( '' );
        dom.XmlNode website_id = data.addChildElement( 'website_id', null, null );
        website_id.addTextNode( '' );
        dom.XmlNode store_id = data.addChildElement( 'store_id', null, null );
        store_id.addTextNode( '' );
        dom.XmlNode group_id = data.addChildElement( 'group_id', null, null );
        group_id.addTextNode( '' );
        dom.XmlNode prefix = data.addChildElement( 'prefix', null, null );
        prefix.addTextNode( '' );
        dom.XmlNode suffix = data.addChildElement( 'suffix', null, null );
        suffix.addTextNode( '' );
        dom.XmlNode dob = data.addChildElement( 'dob', null, null );
        dob.addTextNode( '' );
        dom.XmlNode taxvat = data.addChildElement( 'taxvat', null, null );
        taxvat.addTextNode( Util.getString( account.CPF__c ) );
        dom.XmlNode gender = data.addChildElement( 'gender', null, null );
        gender.addTextNode( '' );
        dom.XmlNode sfid = data.addChildElement( 'sfid', null, null );
        sfid.addTextNode( account.Id );

        return doc.toXmlString();
    }


}