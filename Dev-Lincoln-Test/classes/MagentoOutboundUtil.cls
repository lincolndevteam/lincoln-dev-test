/**
*    @author Diego Moreira
*    @class Classe util de geração de dados de saida
*/
public class MagentoOutboundUtil {
	/**
    *   Retorna o XML para login no magento
    */
    public static String getXMLLogin() {
		String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
		String xsi = 'http://www.w3.org/2001/XMLSchema-instance';
		String xsd = 'http://www.w3.org/2001/XMLSchema';
		String urn = 'urn:Magento';

		DOM.Document doc = new DOM.Document();
		dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');
		envelope.setNamespace( 'xsi', xsi );
		envelope.setNamespace( 'xsd', xsd );
		envelope.setNamespace( 'urn', urn );

		dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
		dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
		dom.XmlNode login = body.addChildElement('login', urn, null);

		dom.XmlNode username = login.addChildElement( 'username', null, null);
		username.addTextNode( Setup__c.getValues( 'MAGENTO_USERNAME' ).Value__c );
		dom.XmlNode apiKey = login.addChildElement( 'apiKey', null, null);
		apiKey.addTextNode( Setup__c.getValues( 'MAGENTO_APIKEY' ).Value__c );

		return doc.toXmlString();
    }

    /**
	*	XML de retorno da integração de login com magento
	*/
	public static String getXMLLoginResponse_Test() {
		String xmlLogin = MagentoOutboundUtil.getXMLLogin();

		return '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:Magento" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">' +
				   '<SOAP-ENV:Body>' +
				      '<ns1:loginResponse>' +
				         '<loginReturn xsi:type="xsd:string">4b98ede78bd018d4f126dcc0d3ffde5c</loginReturn>' +
				      '</ns1:loginResponse>' +
				   '</SOAP-ENV:Body>' + 
				'</SOAP-ENV:Envelope>';
	}

	/*
	*	XML de retorno da integração de cliente com magento
	*/
	public static String getXMLCustomerResponse_Test() {
		return '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:Magento" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">' +
				   '<SOAP-ENV:Body>' +
				      '<ns1:customerCustomerCreateResponse>' +
				         '<result xsi:type="xsd:int">2</result>' +
				      '</ns1:customerCustomerCreateResponse>' +
				   '</SOAP-ENV:Body>' +
				'</SOAP-ENV:Envelope>'; 
	}

}