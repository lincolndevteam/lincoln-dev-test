/**
*   @author Diego Moreira
*   @class Classe DAO do objeto hub service
*/
public with sharing class HubServiceDAO {
    /*
        Singleton
    */  
    private static final HubServiceDAO instance = new HubServiceDAO();    
    private HubServiceDAO(){}
    
    public static HubServiceDAO getInstance() {
        return instance;
    }

    /**
    *
    *
    */
    public List<HubService__c> getRegistrosDuplicados( List<String> classificacao, List<String> motivoCaso, 
                                                        List<String> conteudo, List<String> subMotivo, List<String> tipo, List<String> recordType ) {
        return [SELECT Id, Name, Classificacao__c, MotivoCaso__c, Conteudo__c, Submotivo__c, Tipo__c, RecordTypeId FROM HubService__c 
                    WHERE Classificacao__c IN :classificacao
                    AND MotivoCaso__c IN :motivoCaso
                    AND Conteudo__c IN :conteudo
                    AND Submotivo__c IN :subMotivo
                    AND Tipo__c IN :tipo
                    AND recordTypeid IN :recordType];
    }

    /**
    *
    *
    */
    public List<HubService__c> getRegistrosDuplicados( List<String> hubId, List<String> classificacao, List<String> motivoCaso, 
                                                        List<String> conteudo, List<String> subMotivo, List<String> tipo, List<String> recordType ) {
        return [SELECT Id, Name, Classificacao__c, MotivoCaso__c, Conteudo__c, Submotivo__c, Tipo__c, RecordTypeId FROM HubService__c 
                    WHERE Classificacao__c IN :classificacao
                    AND MotivoCaso__c IN :motivoCaso
                    AND Conteudo__c IN :conteudo
                    AND Submotivo__c IN :subMotivo
                    AND Tipo__c IN :tipo
                    AND Id NOT IN :hubId
                    AND recordTypeid IN :recordType];
    }    
}