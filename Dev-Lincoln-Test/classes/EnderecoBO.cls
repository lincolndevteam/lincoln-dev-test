/**
*    @author Diego Moreira
*    @class Classe de negocio de endereço
*/
public with sharing class EnderecoBO {
	/*
        Singleton
    */
    private static final EnderecoBO instance = new EnderecoBO();    
    private EnderecoBO(){}
    
    public static EnderecoBO getInstance() {
        return instance;
    }

    /**
    *   Cria fila de integração 
    *   @param address Endereços criadas ou atualizadas
    */
    public void criaFilaIntegracao( List<Endereco__c> addressList ) {
        List<String> addressIds = new List<String>();
        List<Queue__c> queueToInsert = new List<Queue__c>();

        for( Endereco__c address : addressList ) {
            addressIds.add( address.Id );
        }
       
        if( !addressIds.isEmpty() ) {
            for( Endereco__c address : EnderecoDAO.getInstance().getEnderecoById( addressIds ) ) {
                if( address.Conta__r.RecordType.DeveloperName.equals( 'BR_Jolivi' ) && 
                        String.isNotBlank( Util.getString( address.Conta__r.CodigoMagento__c ) ) ) {           
                	String xmlResult;
                	if( Util.getString( address.CodigoMagento__c ).equals( '' ) ) {
	                    xmlResult = EnderecoOutboundBO.createEnderecoRequest( address );
	                	queueToInsert.add( QueueBO.getInstance().sObjectQueue( address.Id, QueueEventNames.INSERT_ADDRESS_SF_TO_MAGENTO.name(), xmlResult, false ) );                
	                } else {
	                    xmlResult = EnderecoOutboundBO.updateEnderecoRequest( address );
	            		queueToInsert.add( QueueBO.getInstance().sObjectQueue( address.Id, QueueEventNames.UPDATE_ADDRESS_SF_TO_MAGENTO.name(), xmlResult, false ) );
	                }
                }
            }
            insert queueToInsert;           
        }
    }

    public void updateAccountAddress( List<Endereco__c> addressList ){
        String padraoAddress = '';
        String accountId     = '';
        String addressId     = '';

        List<Endereco__c>   addressToUpdate = new List<Endereco__c>();
        List<Account>       accountToUpdate = new List<Account>();
        List<String>        addressIds = new List<String>();

        for( Endereco__c address : addressList ) {
            addressIds.add( address.Id );
        }

        for( Endereco__c thisAddress : EnderecoDAO.getInstance().getEnderecoById( addressIds ) ) {
            if( thisAddress.Conta__r.RecordType.DeveloperName.equals( 'BR_Jolivi' ) ) {
                addressId = thisAddress.Id;
                if( thisAddress.Conta__c != NULL )
                     accountId = thisAddress.Conta__c ;
                if( thisAddress.PadraoCobranca__c == TRUE )
                    padraoAddress = 'C';
                if( thisAddress.PadraoEntrega__c  == TRUE )
                    padraoAddress += 'E';

                List<Endereco__c>   addressCobranca = EnderecoDAO.getInstance().getAddressByPadraoC( addressId, accountId );
                List<Endereco__c>   addressEntrega  = EnderecoDAO.getInstance().getAddressByPadraoE( addressId, accountId ); 
                List<Endereco__c>   addressAll      = EnderecoDAO.getInstance().getAddressByPadraoCE( addressId, accountId ); 
                List<Account>       account         = EnderecoDAO.getInstance().getAccountById( accountId );

                if( padraoAddress =='C' && addressCobranca.size() >= 1 ){
                    for(Endereco__c enderecosCobranca : addressCobranca){
                        enderecosCobranca.PadraoCobranca__c = FALSE;                   
                        addressToUpdate.add(enderecosCobranca);
                    }
                }

                if( padraoAddress == 'E' && addressEntrega.size() >= 1 ){
                    for(Endereco__c enderecosEntrega : addressEntrega){
                        enderecosEntrega.PadraoEntrega__c = FALSE;
                        addressToUpdate.add(enderecosEntrega);
                    }
                }

                if( padraoAddress == 'CE' ){
                    if( !addressAll.isEmpty() ){
                        for(Endereco__c enderecosCE : addressAll){
                            enderecosCE.PadraoEntrega__c  = FALSE;
                            enderecosCE.PadraoCobranca__c = FALSE;
                            addressToUpdate.add(enderecosCE);
                        }
                    }else{
                        for(Endereco__c enderecosCobranca : addressCobranca){
                            enderecosCobranca.PadraoCobranca__c = FALSE;                   
                            addressToUpdate.add(enderecosCobranca);
                        }
                        for(Endereco__c enderecosEntrega : addressEntrega){
                            enderecosEntrega.PadraoEntrega__c = FALSE;
                            addressToUpdate.add(enderecosEntrega);
                        }
                    }       
                }

                for(Account thisAccount : account){
                    if(padraoAddress.contains('E')){
                        thisAccount.ShippingStreet       = thisAddress.Name + (thisAddress.Numero__c != NULL ? +', '+ thisAddress.Numero__c : ' ') + (thisAddress.Bairro__c != NULL ? +' - '+ thisAddress.Bairro__c : ' ');
                        thisAccount.ShippingCity         = Util.getString(thisAddress.Cidade__c);
                        thisAccount.ShippingState        = Util.getString(thisAddress.Estado__c);
                        thisAccount.ShippingCountry      = Util.getString(thisAddress.Pais__c);
                        thisAccount.ShippingPostalCode   = Util.getString(thisAddress.CEP__c); 
                    }
                    if(padraoAddress.contains('C')){           
                        thisAccount.BillingStreet       = thisAddress.Name + (thisAddress.Numero__c != NULL ? +', '+ thisAddress.Numero__c : ' ') + (thisAddress.Bairro__c != NULL ? +' - '+ thisAddress.Bairro__c : ' ');
                        thisAccount.BillingCity         = Util.getString(thisAddress.Cidade__c);
                        thisAccount.BillingState        = Util.getString(thisAddress.Estado__c);
                        thisAccount.BillingCountry      = Util.getString(thisAddress.Pais__c);
                        thisAccount.BillingPostalCode   = Util.getString(thisAddress.CEP__c);
                        thisAccount.Phone               = Util.getString(thisAddress.Telefone__c);
                    }
                    accountToUpdate.add(thisAccount);
                }
            }
        }

        if(!addressToUpdate.isEmpty()){
            try{
                update addressToUpdate;
            }catch( Exception e ){
                System.debug('ERROR >> ' + e);
            }
        }

        if(!accountToUpdate.isEmpty()){
            try{
                update accountToUpdate;             
            }catch( Exception e ){
                 System.debug('ERROR >> ' + e);
            }
        }
    }
}