/**
*   @author Diego Moreira
*   @class Classe DAO do objeto endereço
*/
public with sharing  class EnderecoDAO {
	/*
        Singleton
    */  
    private static final EnderecoDAO instance = new EnderecoDAO();    
    private EnderecoDAO(){}
    
    public static EnderecoDAO getInstance() {
        return instance;
    }

    /**
    *	Retorna os dados de endereços 
    *	@param addressId Lista de id para consulta
    */
    public List<Endereco__c> getEnderecoById( List<String> addressId ) {
    	return [SELECT Id, Name, CodigoMagento__c, Conta__r.CodigoMagento__c, Conta__r.FirstName, Conta__r.LastName, Conta__r.RecordType.DeveloperName,
                Cidade__c, Pais__c, Cep__c, Estado__c, Telefone__c, PadraoCobranca__c, PadraoEntrega__c, Numero__c, Complemento__c, Bairro__c, Conta__c
                FROM Endereco__c
    			WHERE Id in :addressId];
    }

    public List<Endereco__c> getAddressByPadraoC( String addressId, String accountId ){
        return [SELECT ID FROM Endereco__c WHERE ID !=:addressId AND Conta__c = :accountId AND PadraoCobranca__c = TRUE];
    }

    public List<Endereco__c> getAddressByPadraoE( String addressId, String accountId ){
        return [SELECT ID FROM Endereco__c WHERE ID !=:addressId AND Conta__c = :accountId AND PadraoEntrega__c = TRUE];
    }

    public List<Endereco__c> getAddressByPadraoCE( String addressId, String accountId ){
        return [SELECT ID FROM Endereco__c WHERE ID !=:addressId AND Conta__c = :accountId AND PadraoEntrega__c = TRUE AND PadraoCobranca__c = TRUE];
    }

    public List<Account> getAccountById( String accountId ){
        return [SELECT ID, Phone, BillingStreet, BillingState, BillingCity, BillingCountry, BillingPostalCode, ShippingStreet, ShippingState, ShippingCity, ShippingCountry, ShippingPostalCode FROM Account WHERE ID = :accountId ];
    }
}