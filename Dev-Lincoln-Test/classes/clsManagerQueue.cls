public class clsManagerQueue {
    public List<Group> lstGroupQueue{get;set;}
    /*
Select id,name,developername,email,(select SobjectType,Queueid 
from QueueSobjects),lastmodifiedby.name,lastmodifieddate,type from group where type='Queue'
*/
    public clsManagerQueue (){
        lstGroupQueue = GetGroup();
    }
    public pagereference Novo(){
        PageReference ref = new PageReference('/apex/DetFilas');
        ref.setRedirect(true);
        return ref;
    }
    private list<Group> GetGroup(){
        List<Group> lstGroup = [Select id,name,developername,email,
                                (select SobjectType,Queueid 
                                 from QueueSobjects),
                                lastmodifiedby.name,lastmodifieddate,type 
                                from group where type='Queue'];
        return lstGroup;
    } 
}