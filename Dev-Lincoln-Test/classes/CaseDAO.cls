/*
    @author Lincoln Soares
    @class Classe DAO do objeto Case
*/
public with sharing class CaseDAO {
    /*
        Singleton
    */  
    private static final CaseDAO instance = new CaseDAO();    
    private CaseDAO(){}
    
    public static CaseDAO getInstance() {
        return instance;
    }
    //captura contato pelo @param email
    public List<Contact> getContactByEmail(String email){
        return [SELECT Id FROM Contact 
                WHERE Email = :email];
    }

    //captura conta pelo @param email
    public List<Account> getAccounttByEmail(String email){
        return [SELECT Id FROM Account 
                WHERE Email__c = :email];
    }

    public List<Case> getCasesByIds(List<String> caseIds){
        return [SELECT SuppliedEmail, Classificacao__c, AtendimentoInterno__c, RegraEmailAtiva__c, Origin, Subject, Description FROM Case 
                WHERE Id IN :caseIds];
    }

    public List<Case> getCaseById(String caseId){
        return [SELECT SuppliedEmail, Classificacao__c, AtendimentoInterno__c, RegraEmailAtiva__c, Origin, Subject, Description FROM Case 
                WHERE Id = :caseId];
    }

    public Integer getCountActiveCasesAgente( String ownerId, String idCase ){
        return [SELECT count() FROM Case 
                WHERE OwnerId = :ownerId AND Status = 'Aberto' AND ID != :idCase];
    }

    public List<GroupMember> getQueuesByAgente( String ownerId ){
        return [Select UserOrGroupId, GroupId, Group.Type, Group.Name From GroupMember 
                WHERE Group.Type = 'Queue' AND UserOrGroupId = :ownerId];
    }

    public List<Case> getCasesByQueue( List<String> queues, Integer qtdCases ){
        return [SELECT Id, OwnerId, CreatedDate FROM Case 
                WHERE OwnerId IN :queues AND Status='Aberto' Order By CreatedDate limit :qtdCases];
    }

    /*public Integer getCountCasesByOwner( String ownerId, String caseId ){
        return [SELECT count() FROM CASE
                WHERE (OWNER.PROFILE.NAME = 'Empiricus - Agente de Atendimento' OR OWNER.PROFILE.NAME = 'Empiricus - Agente de Atendimento') AND ID =:caseId AND Ownerid =:ownerId ];
    }*/

    public Integer getCountCasesByOwnerId( String ownerId ){
        return [SELECT count() FROM CASE
                WHERE Status='Aberto' AND Ownerid =:ownerId ];
    }
}