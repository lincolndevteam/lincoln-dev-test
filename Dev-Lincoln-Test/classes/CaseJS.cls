global class CaseJS {

    public CaseJS() {}

    webservice static void addCaseToSpamEmailRule(String caseId){
        CaseBO.getInstance().addCaseToSpamEmailRule(caseId);
    }
    
    webservice static void addCaseToNaoRequerRespEmailRule(String caseId){
        CaseBO.getInstance().addCaseToNaoRequerRespEmailRule(caseId);
    }
}