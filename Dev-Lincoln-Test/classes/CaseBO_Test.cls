@isTest(SeeAllData=TRUE)
private class CaseBO_Test {
  
  @isTest static void test_method_one() {
        Account accountInsertMagento = SObjectInstanceToClassCoverage.accountInstance( null );
        insert accountInsertMagento;

        QueueBO.getInstance().sObjectQueue( accountInsertMagento.Id, QueueEventNames.INSERT_ACCOUNT_SF_TO_MAGENTO.name(), 'XML', true );
    }
    
    @isTest static void test_method_two() {
        QueueBO.getInstance().insertQueue( QueueEventNames.INSERT_ACCOUNT_SF_TO_MAGENTO.name(), 'XML', true );
    }

    @isTest static void test_method_three() {
        String queueId = QueueBO.getInstance().insertQueue( QueueEventNames.INSERT_ACCOUNT_SF_TO_MAGENTO.name(), 'XML', true );

        QueueBO.getInstance().updateQueue( queueId, '' );
    }

    @isTest static void test_method_four() {
        String queueId = QueueBO.getInstance().insertQueue( QueueEventNames.INSERT_ACCOUNT_SF_TO_MAGENTO.name(), 'XML', true );

        QueueBO.getInstance().updateQueue( queueId, '', '' );
    }

    @isTest static void test_method_five() {
        QueueBO.getInstance().reprocessQueue();
    }

    @isTest static void test_method_six() {
        QueueDAO.getInstance().getQueueById('ID');
        QueueDAO.getInstance().getQueueByStatus('CREATED');
    }

    @isTest static void test_method_case1() {
        List<String> lstCaseId = new List<String>();
        Case thisCase = SObjectInstanceToClassCoverage.caseInstance();
        insert thisCase;
        update thisCase;
        
        thisCase.Status = 'Fechado';
        update thisCase;
        lstCaseId.add(thisCase.Id);
        CaseBO.getInstance().setCustomerInCase(lstCaseId);
        CaseBO.getInstance().addCaseToSpamEmailRule(thisCase.Id);
        CaseBO.getInstance().setCustomerInCase(thisCase.Id,FALSE);
        CaseBO.getInstance().setCustomerInCase(thisCase.Id,TRUE);
        CaseBO.getInstance().consultQueuesOfAgent(thisCase.OwnerId, 5);
        
        CaseDAO.getInstance().getContactByEmail('teste@teste.com.br');
        CaseDAO.getInstance().getAccounttByEmail('teste@teste.com.br');
        CaseDAO.getInstance().getCountActiveCasesAgente(thisCase.OwnerId , thisCase.Id);
        CaseDAO.getInstance().getQueuesByAgente(thisCase.OwnerId);
        List<Case> getCases = CaseDAO.getInstance().getCasesByQueue(lstCaseId, 1);
        CaseDAO.getInstance().getCountCasesByOwnerId(thisCase.OwnerId);
        CaseBO.getInstance().updateCaseOwner(getCases ,thisCase.OwnerId, 5);
        
        thisCase.Status = 'Fechado';
        update thisCase;
        
    }
    @isTest static void test_method_case2(){

        Account accountInstanceJoliviBR = SObjectInstanceToClassCoverage.accountInstanceJoliviBR( null );
        insert accountInstanceJoliviBR;

        Contact thisContact = SObjectInstanceToClassCoverage.contactInstance(accountInstanceJoliviBR.Id);
        insert thisContact;
        
        List<Case> lstCases = new List<Case>();

       Case thisCase = SObjectInstanceToClassCoverage.casewithcontactInstance(accountInstanceJoliviBR.Id);
        insert thisCase;
        update thisCase;
        thisCase.Status = 'Aberto';
        thisCase.Type = 'Vendas';
        thisCase.Origin = 'Telefone';
        update thisCase;
        
        thisCase.Status = 'Fechado';
        update thisCase;
        lstCases.add(thisCase);

        Case thisCase2 = SObjectInstanceToClassCoverage.casewithcontactInstance(accountInstanceJoliviBR.Id);
        insert thisCase2;
        update thisCase2;
        thisCase2.Status = 'Aberto';
        thisCase2.Type = 'Vendas';
        thisCase2.Origin = 'Telefone';
        update thisCase2;
        thisCase2.Status = 'Fechado';
        update thisCase2;
        lstCases.add(thisCase2);
        
        CaseBO.getInstance().setCaseToAgent(lstCases);   
    }
    
    @isTest static void test_method_case3() {
        RegraEmail__c emailRule = SObjectInstanceToClassCoverage.emailRules();
        insert emailRule;
        
        CriteriosRegra__c emailCriterio = SObjectInstanceToClassCoverage.emailCriterios( 'teste@teste.com.br', emailRule.Id, 'De:', 'igual a' );
        insert emailCriterio;
        
        CriteriosRegra__c emailCriterio2 = SObjectInstanceToClassCoverage.emailCriterios( 'Subject', emailRule.Id, 'Assunto:', 'igual a' );
        insert emailCriterio2;
        
        CriteriosRegra__c emailCriterio3 = SObjectInstanceToClassCoverage.emailCriterios( 'Description', emailRule.Id, 'Corpo Email:', 'igual a' );
        insert emailCriterio3;
        
        CriteriosRegra__c emailCriterio4 = SObjectInstanceToClassCoverage.emailCriterios( 'teste@teste.com.br', emailRule.Id, 'De:', 'contém' );
        insert emailCriterio4;
        
        CriteriosRegra__c emailCriterio5 = SObjectInstanceToClassCoverage.emailCriterios( 'Subject', emailRule.Id, 'Assunto:', 'contém' );
        insert emailCriterio5;
        
        CriteriosRegra__c emailCriterio6 = SObjectInstanceToClassCoverage.emailCriterios( 'Description', emailRule.Id, 'Corpo Email:', 'contém' );
        insert emailCriterio6;
        
        emailRule.Ativo__c = TRUE;
        update emailRule;
        
        Case thisCase = SObjectInstanceToClassCoverage.caseInstance();
        insert thisCase;
        update thisCase;
        
        thisCase.SuppliedEmail = '';
        update thisCase;
        
        thisCase.Status = 'Fechado';
        update thisCase;
        
    } 
    
    @isTest static void test_method_case4() {
        RegraEmail__c emailRule = SObjectInstanceToClassCoverage.emailRules();
        insert emailRule;
        
        CriteriosRegra__c emailCriterio2 = SObjectInstanceToClassCoverage.emailCriterios( 'Subject', emailRule.Id, 'Assunto:', 'igual a' );
        insert emailCriterio2;
        
        emailRule.Ativo__c = TRUE;
        update emailRule;
        
        Case thisCase = SObjectInstanceToClassCoverage.caseInstance();
        insert thisCase;
        update thisCase;
        
        thisCase.SuppliedEmail = '';
        update thisCase;
        
        thisCase.Status = 'Fechado';
        update thisCase;  
    }      
    
    @isTest static void test_method_case5() {
        RegraEmail__c emailRule = SObjectInstanceToClassCoverage.emailRules();
        insert emailRule;
        
        CriteriosRegra__c emailCriterio3 = SObjectInstanceToClassCoverage.emailCriterios( 'Description', emailRule.Id, 'Corpo Email:', 'igual a' );
        insert emailCriterio3;
        
        emailRule.Ativo__c = TRUE;
        update emailRule;
        
        Case thisCase = SObjectInstanceToClassCoverage.caseInstance();
        insert thisCase;
        update thisCase;
        
        thisCase.SuppliedEmail = '';
        update thisCase;
        
        thisCase.Status = 'Fechado';
        update thisCase;  
    }  

	@isTest static void test_method_case6() {
        RegraEmail__c emailRule = SObjectInstanceToClassCoverage.emailRules();
        insert emailRule;
        
        CriteriosRegra__c emailCriterio4 = SObjectInstanceToClassCoverage.emailCriterios( 'teste@teste.com.br', emailRule.Id, 'De:', 'contém' );
        insert emailCriterio4;
        
        emailRule.Ativo__c = TRUE;
        update emailRule;
        
        Case thisCase = SObjectInstanceToClassCoverage.caseInstance();
        insert thisCase;
        update thisCase;
        
        thisCase.SuppliedEmail = '';
        update thisCase;
        
        thisCase.Status = 'Fechado';
        update thisCase;  
    }  
    
    @isTest static void test_method_case7() {
        RegraEmail__c emailRule = SObjectInstanceToClassCoverage.emailRules();
        insert emailRule;
        
        CriteriosRegra__c emailCriterio5 = SObjectInstanceToClassCoverage.emailCriterios( 'Subject', emailRule.Id, 'Assunto:', 'contém' );
        insert emailCriterio5;
        
        emailRule.Ativo__c = TRUE;
        update emailRule;
        
        Case thisCase = SObjectInstanceToClassCoverage.caseInstance();
        insert thisCase;
        update thisCase;
        
        thisCase.SuppliedEmail = '';
        update thisCase;
        
        thisCase.Status = 'Fechado';
        update thisCase;  
    }          
    
    @isTest static void test_method_case8() {
        RegraEmail__c emailRule = SObjectInstanceToClassCoverage.emailRules();
        insert emailRule;
        
        CriteriosRegra__c emailCriterio6 = SObjectInstanceToClassCoverage.emailCriterios( 'Description', emailRule.Id, 'Corpo Email:', 'contém' );
        insert emailCriterio6;
        
        emailRule.Ativo__c = TRUE;
        update emailRule;
        
        Case thisCase = SObjectInstanceToClassCoverage.caseInstance();
        insert thisCase;
        update thisCase;
        
        thisCase.SuppliedEmail = '';
        update thisCase;
        
        thisCase.Status = 'Fechado';
        update thisCase;  
    }          
}