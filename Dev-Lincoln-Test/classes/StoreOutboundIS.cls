/**
*   @author Diego Moreira
*   @class Classe de integração com o magento
*/
public class StoreOutboundIS{
    /*
        Singleton
    */
    private static final StoreOutboundIS instance = new StoreOutboundIS();    
    private StoreOutboundIS(){}
    
    public static StoreOutboundIS getInstance() {
        return instance;
    }

    /**
    *   Consulta os dados do cliente na Store
    *   @param addressEmail
    */
    public Map<String, Object> getStoreCustomerById( String customerId ) {
        HttpResponse res;
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint( String.format( Setup__c.getValues( 'GET_CUSTOMER_BY_ID' ).Value__c, new String[]{ customerId } ) + '?token=' + Setup__c.getValues( 'STORE_API' ).Value__c  );
        req.setMethod('GET');
        req.setTimeout( 120000 );

        Http h = new Http();
        
        if( Test.isRunningTest() ){ 
            return ( Map<String, Object> ) JSON.deserializeUntyped( returnJSONOrderTest() );
        }else{
            res = h.send( req );   
        }
        System.debug( '>>> ' + res.getBody() );
        if( res.getStatusCode() == 404 ) return null;
        return ( Map<String, Object> ) JSON.deserializeUntyped( res.getBody() );
    }

    /**
    *   Consulta os dados do cliente na Store
    *   @param addressEmail
    */

    public Map<String, Object> getStoreCustomerByEmail( String addressEmail ) {
        HttpResponse res;
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint( Setup__c.getValues( 'GET_STORE_ORDER_DETAIL' ).Value__c + '?email=' + addressEmail + '&token=' + Setup__c.getValues( 'STORE_API' ).Value__c  );
        req.setMethod('GET');
        req.setTimeout( 120000 );

        Http h = new Http();
        
        if( Test.isRunningTest() ){ 
            return ( Map<String, Object> ) JSON.deserializeUntyped( returnJSONOrderTest() );
        }else{
            res = h.send( req );   
        }
        System.debug( '>>> ' + res.getBody() );
        if( res.getStatusCode() == 404 ) return null;
        return ( Map<String, Object> ) JSON.deserializeUntyped( res.getBody() );
    }

    /**
    *   Consulta os dados da assinatura do cliente na Store
    *   @param customerStoreId
    */

    public Map<String, Object> getStoreSubscriptionsByCustomerStoreId( String customerStoreId ) {
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint( Setup__c.getValues( 'GET_STORE_SUBSCRIPTION' ).Value__c + '?customer_id=' + customerStoreId + '&token=' + Setup__c.getValues( 'STORE_API' ).Value__c  );
        req.setMethod('GET');
        req.setTimeout( 120000 );

        Http h = new Http();
        HttpResponse res = h.send( req );   

        if( res.getStatusCode() == 404 ) return null;
        return ( Map<String, Object> ) JSON.deserializeUntyped( res.getBody() );
    }
    
    public String returnJSONOrderTest(){
        return '{"meta": {"total_count": 1,"previous": false,"limit": 30,"page": 1,"next": false},"objects": [{"total_spent": 0,"subscriptions_total": 0,"name": "Rogério Luiz Batista De Oliveira","subscriptions": [],"mobile": "11-99610-4536","id": 265184,"phone": "11-99610-4536","cpf_cnpj": "275.325.968-26","orders": [],"email": "rogerio@beecloud.com.br","date_birthday": "17/05/1979"}]}';    
    }
}