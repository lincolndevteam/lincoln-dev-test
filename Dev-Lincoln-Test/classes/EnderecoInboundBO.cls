/**
*   @author Diego Moreira
*   @class Classe de negocio da integração de endereço
*/
public class EnderecoInboundBO implements IQueueProcessing {
	/** 
    *   Processamento da fila 
    *	@param queueId Id da fila de processamento 
    *	@param recordId Id de registro	
    *   @param eventName nome do evento de processamento
    *   @param payload JSON com o item da fila para processamento
    */     
    public static void execute( String queueId, String recordId, String eventName, String payload ) {
        Savepoint sp = Database.setSavepoint();
        Database.SaveResult[] dmlResult; 
        try {
            if( eventName.equals( QueueEventNames.INSERT_ADDRESS_MAGENTO_TO_SF.name() ) ) {
                dmlResult = insertAddress( payload );       
            } else if( eventName.equals( QueueEventNames.UPDATE_ADDRESS_MAGENTO_TO_SF.name() ) ) {
                dmlResult = updateAddress( payload ); 
            }

            QueueBO.getInstance().updateQueue( queueId, response( dmlResult ), '' );
        } catch( Exception ex ) {
            Database.rollback(sp);
            QueueBO.getInstance().updateQueue( queueId, Util.jsonGenericResponse( ex.getMessage() ), eventName + ' / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
        }
    }

    /**
    *   Metodo de criação de endereço
    *   @param payload JSON com as informações 
    */
    private static Database.SaveResult[] insertAddress( String payload ) {
        List<Endereco__c> addressToInsert = new List<Endereco__c>();        
        Map<String, Object> mapResult = ( Map<String, Object> ) JSON.deserializeUntyped( payload );

        for( Object objResult : ( List<Object> ) mapResult.get( 'address' ) ) {
            Map<String, Object> mapAddress = ( Map<String, Object> ) objResult;
            Endereco__c address 		= new Endereco__c();
            address.Conta__c        	= Util.getString( mapAddress.get( 'customerid' ) );
            address.Name            	= Util.getString( mapAddress.get( 'endrua' ) );
            address.Bairro__c       	= Util.getString( mapAddress.get( 'endbairro' ) );
            address.Cep__c          	= Util.getString( mapAddress.get( 'endcep' ) );
            address.Cidade__c 			= Util.getString( mapAddress.get( 'endcidade' ) );
            address.Complemento__c 		= Util.getString( mapAddress.get( 'endcomplemento' ) );
            address.Estado__c       	= Util.getString( mapAddress.get( 'endestado' ) );
            address.Numero__c       	= Util.getString( mapAddress.get( 'endnumero' ) );
            address.PadraoCobranca__c   = Util.getBoolean( mapAddress.get( 'isdefaultbilling' ) );
            address.PadraoEntrega__c    = Util.getBoolean( mapAddress.get( 'isdefaultshipping' ) );
            address.Pais__c         	= Util.getString( mapAddress.get( 'endpais' ) );
            address.CodigoMagento__c 	= Util.getString( mapAddress.get( 'codigomagento' ) );
            address.Telefone__c         = Util.getString( mapAddress.get( 'telefone' ) );

        	addressToInsert.add( address );
        }

        Database.SaveResult[] result = Database.insert( addressToInsert, false );
        return result;
    }

    /**
    *   Metodo de atualização de endereço
    *   @param payload JSON com as informações 
    */
    private static Database.SaveResult[] updateAddress( String payload ) {
        List<Endereco__c> addressToInsert = new List<Endereco__c>();        
        Map<String, Object> mapResult = ( Map<String, Object> ) JSON.deserializeUntyped( payload );

        for( Object objResult : ( List<Object> ) mapResult.get( 'address' ) ) {
            Map<String, Object> mapAddress = ( Map<String, Object> ) objResult;
            Endereco__c address 		= new Endereco__c();
            address.Id            		= Util.getString( mapAddress.get( 'sfid' ) );
            address.Conta__c            = Util.getString( mapAddress.get( 'customerid' ) );
            address.Name            	= Util.getString( mapAddress.get( 'endrua' ) );
            address.Bairro__c       	= Util.getString( mapAddress.get( 'endbairro' ) );
            address.Cep__c          	= Util.getString( mapAddress.get( 'endcep' ) );
            address.Cidade__c 			= Util.getString( mapAddress.get( 'endcidade' ) );
            address.Complemento__c 		= Util.getString( mapAddress.get( 'endcomplemento' ) );
            address.Estado__c       	= Util.getString( mapAddress.get( 'endestado' ) );
            address.Numero__c       	= Util.getString( mapAddress.get( 'endnumero' ) );
            address.PadraoCobranca__c   = Util.getBoolean( mapAddress.get( 'isdefaultbilling' ) );
            address.PadraoEntrega__c    = Util.getBoolean( mapAddress.get( 'isdefaultshipping' ) );
            address.Pais__c         	= Util.getString( mapAddress.get( 'endpais' ) );
            address.CodigoMagento__c    = Util.getString( mapAddress.get( 'codigomagento' ) );
            address.Telefone__c         = Util.getString( mapAddress.get( 'telefone' ) );

        	addressToInsert.add( address );
        }

        Database.SaveResult[] result = Database.update( addressToInsert, false );
        return result;
    }

    /**
    *   Retorna modelo dados apos integração
    *   @param dmlResult resultado de inserção na base de dados
    */
    private static String response( Database.SaveResult[] dmlResult ) {
    	Map<String, String> mapSObject = new Map<String, String>();
        List<String> idList = new List<String>();
        for( Database.SaveResult result : dmlResult )
            idList.add( Util.getString( result.getId() ) );

        for( Endereco__c address : EnderecoDAO.getInstance().getEnderecoById( idList ) )
            mapSObject.put( address.Id, address.CodigoMagento__c );

        return Util.jsonResponse( dmlResult, mapSObject );
    }
}