/*
    @author Lincoln Soares
    @class Classe de negocio do objeto Case
*/
public with sharing class CaseBO {
    /*
        Singleton
    */
    private static final CaseBO instance = new CaseBO();    
    private CaseBO(){}
    
    public static CaseBO getInstance() {
        return instance;
    }
    
    public void setCustomerInCase(List<String> caseIds){
        String contactId = '';
        String accountId = '';
        List<String> emails = new List<String>();
        List<Case> updateCase = new List<Case>();
        Boolean existsCustomer = false;
        ProcessControl.ignoredByTrigger = True;

        for(Case thisCase : CaseDAO.getInstance().getCasesByIds(caseIds)){
            emails.add(thisCase.SuppliedEmail);    
        }
        
        List<Account> accounts = AccountDAO.getInstance().getAccountsByEmails( emails );
        List<Contact> contacts = ContactDAO.getInstance().getContactsByEmails( emails );

        for(Case thisCase : CaseDAO.getInstance().getCasesByIds(caseIds)){
            for(Contact ctt : contacts){
                if(thisCase.SuppliedEmail == ctt.Email)
                    thisCase.ContactId = ctt.Id;
                    existsCustomer = true;
            }   
            for(Account act : accounts){
                if(thisCase.SuppliedEmail == act.PersonEmail)
                    thisCase.AccountId = act.Id;
                    existsCustomer = true;
            }
            
            if(!existsCustomer){
                thisCase.Classificacao__c = 'Free';
            }
            updateCase.add(thisCase);
        }

        Update updateCase;
    }

    /**
    *
    * @To-do 
    *   - Pesquisar por oferta de maior valor
    *   - Pesquisa por classificação do caso
    */
    public void setCustomerInCase(String caseId, Boolean isCustomer){
        String contactId = '';
        String accountId = '';
        ProcessControl.ignoredByTrigger = True;

        List<String> caseIdList = caseId.split( ',' );

        for( Case thisCase : CaseDAO.getInstance().getCasesByIds( caseIdList ) ) {
            if( !isCustomer ) {
                thisCase.Classificacao__c = 'Free';
                thisCase.AtribuirUsandoRegraAtiva__c = thisCase.RegraEmailAtiva__c == true ? false : true;
                update thisCase;
            } else {
                  
                List<Contact> contacts = ContactDAO.getInstance().getContactsByEmails( new List<String>{ thisCase.SuppliedEmail } );
                thisCase.AtribuirUsandoRegraAtiva__c = thisCase.RegraEmailAtiva__c == true ? false : true;

                for(Contact contact : contacts){
                    if(thisCase.SuppliedEmail == contact.Email) {
                        thisCase.ContactId = contact.Id;
                        thisCase.AccountId = contact.AccountId;
                    }                    
                }
               
                Boolean isSubscriptionActive = false;
                Boolean isSubscription = false;

                for( Assinatura__c ass : [Select Status__c from Assinatura__c where Cliente__c = :thisCase.AccountId] ){
                    isSubscription = true;
                    if( ass.Status__c == 'ATIVA' )
                        isSubscriptionActive = true;
                }

                if( isSubscription == false && isSubscriptionActive == false )
                    thisCase.Classificacao__c = 'Free';

                if( isSubscription == true && isSubscriptionActive == false ){
                    thisCase.Classificacao__c = 'Free - Antigo Assinante';
                }else{
                    Map<String, String> mapRecordType = new Map<String, String>();

                    for( RecordType recordType : RecordTypeDAO.getInstance().getRecordTypeByDeveloperName( 'Product2' ) )
                        mapRecordType.put( recordType.DeveloperName, recordType.Id );

                    for ( Assinatura__c ass : [Select PlanoID__c, NomePlano__c, TipoServico__c from Assinatura__c where Cliente__c = :thisCase.AccountId and Status__c = 'ATIVA' order by Preco__c desc limit 1] ){
                        Product2 prod = new Product2();
                        prod.RecordTypeId = mapRecordType.get( 'BR_Empiricus_Produto' );
                        prod.CodigoStored__c = ass.PlanoID__c;
                        prod.Name = ass.NomePlano__c;
                        prod.ProductCode = ass.PlanoID__c;
                        upsert prod Product.CodigoStored__c;
                        
                        for(Product2 thisProd : ProductDAO.getInstance().getProductByStoredId(ass.PlanoID__c)){
                            thisCase.Classificacao__c = thisProd.Classificacao__c;
                            thisCase.Produto__c = thisProd.Id;
                            thisCase.Conteudo__c = thisProd.Conteudo__c;
                        }
                    }
                }

                if(thisCase.Conteudo__c == null)
                    if(thisCase.Classificacao__c != 'Free' || thisCase.Classificacao__c != 'Free - Antigo Assinante')
                        thisCase.ErroAtribuicao__c = true;
                update thisCase;
            }
        }
  
    }

    public void duplicateCase( List<Case> caseNew ) {
        List<Case> listCases = [Select Id, Status, Type, ContactId, Origin From Case Where Status = 'Aberto' And Type = 'Vendas' And Origin = 'Telefone'];

        for(Case thisCase : caseNew){
            for(Case cases : listCases){
                if(thisCase.Status == cases.Status && 
                    thisCase.Type == cases.Type && 
                    thisCase.ContactId == cases.ContactId && 
                    thisCase.Origin == cases.Origin){
                    thisCase.adderror('Já existe um caso de Vendas criado para esse Cliente, revise os Casos com Status em Aberto desse Cliente!');
                }
            }
        }
    }

    /**
    *   Atualiza o email do yahoo
    *   @param caseList Lista de casos para verificação e correção do email
    */
    public void updateEmail( List<Case> caseList ) {
        for( Case case2 : caseList ) {
            if( case2.SuppliedEmail != null &&
                    case2.SuppliedEmail.contains( '.yahoo.' ) ) {
                case2.SuppliedEmail = Util.updateYahooMail( case2.SuppliedEmail );
            }
        }
    }

    public void addCaseToSpamEmailRule(String caseId){
        Case thisCase = CaseDAO.getInstance().getCaseById(caseId)[0];

        Group fila = [SELECT Id, Name FROM Group WHERE Name = 'Relacionamento - Spam'];
        /*
        Boolean ruleExists = false;
        Id regraEmail;

        List<CriteriosRegra__c> emailRules = [SELECT RegradeEmail__r.OwnerId, RegradeEmail__c, Operador__c, Campo__c, Valor__c FROM CriteriosRegra__c
                                                    WHERE RegradeEmail__r.Ativo__c = True
                                                    AND RegradeEmail__r.DefaultRuleSpam__c = True];

        for(CriteriosRegra__c emailRule : emailRules) {
            regraEmail = emailRule.RegradeEmail__c;
            if(thisCase.SuppliedEmail == emailRule.Valor__c){
                ruleExists = true;      
            }
        }       

        if(ruleExists == false && regraEmail != null){
            CriteriosRegra__c insertEmailRule = new CriteriosRegra__c();
            insertEmailRule.RegradeEmail__c = regraEmail;
            insertEmailRule.Operador__c = 'igual a';
            insertEmailRule.Valor__c = thisCase.SuppliedEmail;
            insertEmailRule.Campo__c = 'De:';
            insert insertEmailRule;
        }
        */

        List<Case> listCase = new List<Case>();
        thisCase.OwnerId = fila.Id;
        thisCase.VerifiedEmailRule__c = false;
        listCase.add(thisCase);
        //emailRules(listCase);
        update listCase;

    }
    
    public void addCaseToNaoRequerRespEmailRule( String caseId ){
        Case thisCase = CaseDAO.getInstance().getCaseById(caseId)[0];

        Group fila = [SELECT Id, Name FROM Group WHERE Name = 'Relacionamento - Não Requer Resposta'];

        List<Case> listCase = new List<Case>();
            thisCase.OwnerId = fila.Id;
            thisCase.VerifiedEmailRule__c = false;
            listCase.add(thisCase);
        
        update listCase;
    }
  
    public void emailRules(List<Case> listCase){
        Boolean activeRule = false;
        List<Case> updateCase = new List<Case>();

        List<CriteriosRegra__c> emailRules = [SELECT RegradeEmail__r.OwnerId, RegradeEmail__c, Operador__c, Campo__c, Valor__c FROM CriteriosRegra__c
                                                    WHERE RegradeEmail__r.Ativo__c = True
                                                    ORDER BY RegradeEmail__r.Sequencia__c];
        
        for(Case thisCase : listCase){
            if(thisCase.VerifiedEmailRule__c == false && thisCase.SuppliedEmail != null){
                if(thisCase.Origin.contains('Email')){
                    for( CriteriosRegra__c thisEmailRule : emailRules ){
                        if( thisEmailRule.Operador__c == 'igual a' ){
                            
                            if(thisEmailRule.Campo__c == 'De:'){
                                if( thisCase.SuppliedEmail == thisEmailRule.Valor__c){
                                    thisCase.RegraEmailAtiva__c = true;
                                    thisCase.RegraEmail__c = thisEmailRule.RegradeEmail__c;
                                    updateCase.add(thisCase);
                                    System.debug( '>>>DE: igual a ' + thisCase );
                                    break;
                                }
                            }

                            if(thisEmailRule.Campo__c == 'Assunto:'){
                                if( thisCase.Subject == thisEmailRule.Valor__c){
                                    thisCase.RegraEmailAtiva__c = true;
                                    thisCase.RegraEmail__c = thisEmailRule.RegradeEmail__c;
                                    updateCase.add(thisCase);
                                    System.debug( '>>>ASSUNTO: igual a ' + thisCase );
                                    break;
                                }
                            }

                            if(thisEmailRule.Campo__c == 'Corpo Email:'){
                                if( thisCase.Description == thisEmailRule.Valor__c){
                                    thisCase.RegraEmailAtiva__c = true;
                                    thisCase.RegraEmail__c = thisEmailRule.RegradeEmail__c;
                                    updateCase.add(thisCase);
                                    System.debug( '>>>CORPO: igual a ' + thisCase );
                                    break;
                                }
                            }                            

                        }else if(thisEmailRule.Operador__c == 'contém'){
                               
                            if(thisEmailRule.Campo__c == 'De:'){
                                if(thisCase.SuppliedEmail != null && thisCase.SuppliedEmail.contains(thisEmailRule.Valor__c)){
                                    thisCase.RegraEmailAtiva__c = true;
                                    thisCase.RegraEmail__c = thisEmailRule.RegradeEmail__c;
                                    updateCase.add(thisCase);
                                    System.debug( '>>>DE: contém ' + thisCase );
                                    break;   
                                }
                            }
                            
                            if(thisEmailRule.Campo__c == 'Assunto:'){
                                if(thisCase.Subject != null && thisCase.Subject.contains(thisEmailRule.Valor__c)){
                                    thisCase.RegraEmailAtiva__c = true;
                                    thisCase.RegraEmail__c = thisEmailRule.RegradeEmail__c;
                                    updateCase.add(thisCase);
                                    System.debug( '>>>ASSUNTO: contém ' + thisCase );
                                    break;   
                                }
                            }

                            if(thisEmailRule.Campo__c == 'Corpo Email:'){
                                if(thisCase.Description != null && thisCase.Description.contains(thisEmailRule.Valor__c)){
                                    thisCase.RegraEmailAtiva__c = true;
                                    thisCase.RegraEmail__c = thisEmailRule.RegradeEmail__c;
                                    updateCase.add(thisCase);
                                    System.debug( '>>>ASSUNTO: contém ' + thisCase );
                                    break;   
                                }
                            }
                        }  
                    }
                }
            }
            thisCase.VerifiedEmailRule__c = true;
        }
        //System.debug( '>>> ' + updateCase );
        //ProcessControl.ignoredByTrigger = true;
        //update updateCase;  
    }

    public void criaFilaIntegracao(List<Case> cases){
        List<String> caseIds                    = new List<String>();
        List<String> lstTiposdeRegistroAtivos   = new List<String>();
        Set<String>  setLstCtrlIntegracao       = new Set<String>();

        List<ControleIntegracaoCaso__c> controleIntegracaoCaso = ControleIntegracaoCaso__c.getall().values();  

        for( ControleIntegracaoCaso__c rtAtivo : controleIntegracaoCaso ){
            lstTiposdeRegistroAtivos.add( rtAtivo.NomeTipoRegistrosAtivo__c );
        }

        if( !lstTiposdeRegistroAtivos.isEmpty() ){
            setLstCtrlIntegracao.addAll(lstTiposdeRegistroAtivos);
            for( Case c : cases ) {
                if(c.SuppliedEmail != null && setLstCtrlIntegracao.contains(c.RecordTypeDevName__c) )
                    caseIds.add( c.Id );
            }
        }

        if( !caseIds.isEmpty() )
            criaFilaIntegracao(caseIds);
    }

    /**
    *   Cria fila de integração 
    *   @param cases Contas criadas ou atualizadas
    */
    @Future(Callout=true)
    public static void criaFilaIntegracao( List<String> caseIds ) {       
        List<Queue__c> queueToInsert = new List<Queue__c>();

        if( !caseIds.isEmpty() ) {
            Map<String,Object> response = new Map<String,Object>();
            Map<String, String> mapEmail = new Map<String, String>();

            for( Case thisCase : CaseDAO.getInstance().getCasesByIds(caseIds) ) {
                if( mapEmail.get( thisCase.SuppliedEmail ) == null ) {
                    mapEmail.put( thisCase.SuppliedEmail, thisCase.Id );
                } else {
                    String recordIds = mapEmail.get( thisCase.SuppliedEmail ) + ',' + thisCase.Id;
                    mapEmail.put( thisCase.SuppliedEmail, recordIds ); 
                }                       
            }

            for( String email : mapEmail.keySet() ) {
                response = StoreOutboundIS.getInstance().getStoreCustomerByEmail( email );
                queueToInsert.add(QueueBO.getInstance().sObjectQueue( mapEmail.get( email ), QueueEventNames.QUERY_ACCOUNT_STORE_TO_SALESFORCE.name(), JSON.serialize(response), false));
            }

            insert queueToInsert;    
        }
    }

    public void setCaseToAgent( List<Case> cases ){
        
        for( Case thisCase : cases ){
            Integer qtdCasosAgente  = 0;
            String  ownerId         = '';
            Integer qtdCasosOwner   = 0;
            System.debug( 'thisCase.isClosed >> '           + thisCase.isClosed);
            System.debug( 'thisCase.OWNER.PROFILE.NAME >> ' + thisCase.OwnerProfileName__c );

            List<AtribuicaoAgente__c> atribuicaoAgente = AtribuicaoAgente__c.getall().values();   

            if( thisCase.isClosed && thisCase.OwnerProfileName__c == atribuicaoAgente[0].PerfilAtivo__c ){ //thisCase.isClosed && thisCase.OwnerProfileName__c == atribuicaoAgente[0].PerfilAtivo__c
                ownerId         = thisCase.OwnerId;
                qtdCasosAgente  = CaseDAO.getInstance().getCountActiveCasesAgente( thisCase.OwnerId, thisCase.Id );
                System.debug('qtdCasosAgente >> ' + qtdCasosAgente);
                        
                if( qtdCasosAgente < Integer.valueOf(atribuicaoAgente[0].NumeroCasosPorAgente__c) )
                    consultQueuesOfAgent( ownerId, qtdCasosAgente );
            }
        }
    }

    public void consultQueuesOfAgent( String ownerId, Integer qtdCasosAgente ){

        List<String> queuesToCases          = new List<String>();
        List<Case> casesOfQueues            = new List<Case>();
        List<GroupMember> queuesOfAgente    = CaseDAO.getInstance().getQueuesByAgente( ownerId );
        
        List<AtribuicaoAgente__c> atribuicaoAgente = AtribuicaoAgente__c.getall().values();

        System.debug('atribuicaoAgente >> ' + atribuicaoAgente);

        Integer qtdCasesToAgente    = Integer.valueOf(atribuicaoAgente[0].NumeroCasosPorAgente__c) - qtdCasosAgente;
        qtdCasesToAgente            = qtdCasesToAgente >= 0 ? qtdCasesToAgente : 0;

        System.debug('qtdCasesToAgente >> ' + qtdCasesToAgente);

        for( GroupMember thisQueue : queuesOfAgente ){
            queuesToCases.add(thisQueue.GroupId);
        }

        System.debug('queuesToCases >> '    + queuesToCases);

        if( !queuesToCases.isEmpty() )
            casesOfQueues = CaseDAO.getInstance().getCasesByQueue( queuesToCases, qtdCasesToAgente );
        if( !casesOfQueues.isEmpty() && atribuicaoAgente[0].Ativo__c )
            updateCaseOwner( casesOfQueues, ownerId, qtdCasosAgente );

        System.debug('casesOfQueues >> '    + casesOfQueues);
    }

    public void updateCaseOwner( List<Case> cases, String ownerId, Integer qtdCasosAgente ){
        List<Case> casesToUpdate = new List<Case>();

        for(Case thisCase : cases){     
            thisCase.OwnerId = ownerId;
            casesToUpdate.add(thisCase);  
        }
        try{
            if( !casesToUpdate.isEmpty() )
                update casesToUpdate;
        }catch( DmlException ex ) {
            System.debug('DmlException >> ' + ex.getMessage());
        }
    }
    
    public void setOwnerUpdate( List<Case> cases, List<Case> casesOld ){
        String ownerId      = '';
        String ownerIdNew   = '';
        for( Case caseOld : casesOld ) {
            if( String.valueOf(caseOld.OwnerId).startsWith('005') && caseOld.OwnerProfileName__c == 'TRUE' )
                ownerIdNew = UserInfo.getUserId();
        }
        if( ownerIdNew != '' ){
            for( Case caseNew : cases ){
                if( String.valueOf(caseNew.OwnerId).startsWith('005') && caseNew.AtribuirUsandoRegraAtiva__c == FALSE  )
                    caseNew.OwnerId = ownerIdNew;
            }
        }
    }
}