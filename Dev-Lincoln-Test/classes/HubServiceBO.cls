/**
*    @author Diego Moreira
*    @class Classe de negocio de hub service
*/
public with sharing class HubServiceBO {
    /*
        Singleton
    */
    private static final HubServiceBO instance = new HubServiceBO();    
    private HubServiceBO(){}
    
    public static HubServiceBO getInstance() {
        return instance;
    }

    /*
    *
    *
    */
    public void verificaDuplicadosCriacao( List<HubService__c> hubServiceList ) {
        List<String> classificacao = new List<String>();
        List<String> motivoCaso = new List<String>();
        List<String> conteudo = new List<String>();
        List<String> subMotivo = new List<String>();
        List<String> tipo = new List<String>();
        List<String> recordType = new List<String>();

        for( HubService__c hubService : hubServiceList ) {
            classificacao.add( hubService.Classificacao__c );
            motivoCaso.add( hubService.MotivoCaso__c );
            conteudo.add( hubService.Conteudo__c );
            subMotivo.add( hubService.Submotivo__c );
            tipo.add( hubService.Tipo__c );
            recordType.add( hubservice.recordTypeId );
        }

        List<HubService__c> resultList = HubServiceDAO.getInstance().getRegistrosDuplicados( classificacao, motivoCaso, conteudo, subMotivo, tipo, recordtype );

        for( HubService__c hubService : hubServiceList ) {
            for( HubService__c result : resultList ) {
                if( hubService.Classificacao__c == result.Classificacao__c &&
                        hubService.MotivoCaso__c == result.MotivoCaso__c  &&
                        hubService.Conteudo__c == result.Conteudo__c  &&
                        hubService.Submotivo__c == result.Submotivo__c  &&
                        hubService.Tipo__c == result.Tipo__c  &&
                        hubService.recordTypeId == result.RecordTypeId){
                    hubService.addError( 'Registro duplicado!' );
                }
            }
        }

    }

    /*
    *
    *
    */
    public void verificaDuplicadosAtualizacao( List<HubService__c> hubServiceList ) {
        List<String> classificacao = new List<String>();
        List<String> motivoCaso = new List<String>();
        List<String> conteudo = new List<String>();
        List<String> subMotivo = new List<String>();
        List<String> tipo = new List<String>();
        List<String> hubId = new List<String>();
        List<String> recordType = new List<String>();

        for( HubService__c hubService : hubServiceList ) {
            classificacao.add( hubService.Classificacao__c );
            motivoCaso.add( hubService.MotivoCaso__c );
            conteudo.add( hubService.Conteudo__c );
            subMotivo.add( hubService.Submotivo__c );
            tipo.add( hubService.Tipo__c );
            hubId.add( hubService.Id );
            recordType.add( hubservice.recordTypeId );
        }

        List<HubService__c> resultList = HubServiceDAO.getInstance().getRegistrosDuplicados( hubId, classificacao, motivoCaso, conteudo, subMotivo, tipo, recordType );

        for( HubService__c hubService : hubServiceList ) {
            for( HubService__c result : resultList ) {
                if( hubService.Classificacao__c == result.Classificacao__c &&
                        hubService.MotivoCaso__c == result.MotivoCaso__c  &&
                        hubService.Conteudo__c == result.Conteudo__c  &&
                        hubService.Submotivo__c == result.Submotivo__c  &&
                        hubService.Tipo__c == result.Tipo__c &&
                        hubService.recordTypeId == result.RecordTypeId) {
                    hubService.addError( 'Registro duplicado!' );
                }
            }
        }

    }
}