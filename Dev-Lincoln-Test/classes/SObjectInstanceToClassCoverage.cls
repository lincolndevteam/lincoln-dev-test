/**
*    @author Diego Moreira
*    @class Classe de instancia de objetos para classe de testes
*/
public class SObjectInstanceToClassCoverage {


    /**
    *
    *
    */
    public static Account accountInstance( String recordId ) {
        Account account = new Account();
        if( String.isNotBlank( Util.getString( recordId ) ) ) account.Id = recordId;
        account.FirstName           = 'Name'; 
        account.LastName            = 'LastName';
        account.Nome_old__c         = 'FirstName';
        account.Sobrenome_old__c    = 'LastName';
        account.CPF__c              = '91938490797';
        account.CNPJ__c             = '54475563000165';
        account.DataNascimento__c   = Date.today();
        account.Email__c            = 'teste@teste.com.br';
        account.Celular__c          = '9988998899';
        account.Type                = 'Pessoa Física';
        account.PersonBirthdate     = Date.today();
        account.RecordTypeId        = Schema.SObjectType.Account.getRecordTypeInfosByName().get( 'Jolivi' ).getRecordTypeId();
        System.debug('account.RecordTypeId >> '+ account.RecordTypeId );

        return account;
    }


    /**
    *
    *
    */
    public static Account accountInstanceJoliviBR( String recordId ) {
        Account account = new Account();
        if( String.isNotBlank( Util.getString( recordId ) ) ) account.Id = recordId;
        account.Name                = 'Name'; 
        account.Nome_old__c         = 'FirstName';
        account.Sobrenome_old__c    = 'LastName';
        account.CPF__c              = '91938490797';
        account.CNPJ__c             = '54475563000165';
        account.DataNascimento__c   = Date.today();
        account.Email__c            = 'teste@teste.com.br';
        account.Type                = 'Pessoa Jurídica';
        account.RazaoSocial__c      = 'razao social';
        account.TradingName__c      = 'nome fantasia';
        account.RecordTypeId        = Schema.SObjectType.Account.getRecordTypeInfosByName().get( 'BR_Jolivi' ).getRecordTypeId();
        System.debug('account.RecordTypeId >> '+ account.RecordTypeId );

        return account;
    }

    /**
    *
    *
    */
    public static Account accountEmpiricusInstance( String recordId ) {
        Account account = new Account();
        if( String.isNotBlank( Util.getString( recordId ) ) ) account.Id = recordId;
        account.FirstName           = 'Name';
        account.LastName            = 'LastName';
        account.Nome_old__c         = 'FirstName';
        account.Sobrenome_old__c    = 'LastName';
        account.CPF__c              = '91938490797';
        account.CNPJ__c             = '54475563000165';
        account.DataNascimento__c   = Date.today();
        account.PersonBirthdate     = Date.today();
        account.Email__c            = 'teste@teste.com.br';
        account.Type                = 'Pessoa Física';
        account.RecordTypeId        = Schema.SObjectType.Account.getRecordTypeInfosByName().get( 'Empiricus' ).getRecordTypeId();
        System.debug('account.RecordTypeId >> '+ account.RecordTypeId );

        return account;
    }


    /**
    *
    *
    */
    public static Contact contactInstance( String accountId ) {
        Contact contact = new Contact();
        contact.FirstName           = 'FirstName';
        contact.LastName            = 'LastName';   
        contact.Email               = 'teste@teste.com.br';
        contact.AccountId           = accountId;
        return contact;
    }



    /**
    *
    *
    */
    public static Endereco__c enderecoInstance( String recordId, String accountId ) {
        Endereco__c endereco = new Endereco__c();
        if( String.isNotBlank( Util.getString( recordId ) ) ) endereco.Id = recordId;
        endereco.Conta__c = accountId;
        endereco.Name = 'Endereço teste';
        endereco.PadraoEntrega__c = true;
        endereco.PadraoCobranca__c  = true;

        return endereco;
    }

    /**
    * 
    *
    */
    public static Oferta__c ofertaInstance() {
        Oferta__c thisOferta = new Oferta__c();
        thisOferta.Name             = 'Oferta';
        thisOferta.Ativa__c         = TRUE;
        thisOferta.ValorOferta__c   = 1000;

        return thisOferta;
    }

    /**
    * 
    *
    */
    public static Oferta__c oferta2Instance() {
        Oferta__c thisOferta = new Oferta__c();
        thisOferta.Name             = 'Oferta2';
        thisOferta.Ativa__c         = TRUE;
        thisOferta.ValorOferta__c   = 100;

        return thisOferta;
    }
    

    /**
    * 
    *
    */
    /*public static Order orderInstance( String accountId, String ofertaId ) {
        Order thisOrder = new Order();
        thisOrder.AccountId     = accountId;
        thisOrder.Status        = 'Novo';
        thisOrder.EffectiveDate = System.today();
        thisOrder.Oferta__c     = ofertaId;

        return thisOrder;
    } */
    
    
    /**
    * 
    *
    */
    public static RegraEmail__c emailRules() {
        RegraEmail__c emailRule = new RegraEmail__c();
        emailRule.Ativo__c      = FALSE;
        emailRule.Name          = 'Teste Nome';
        emailRule.Sequencia__c  = 1;

        return emailRule;
    }
    
    /**
    * 
    *
    */
    public static CriteriosRegra__c emailCriterios(String valor, String regraEmail, String campo, String operador) {
        CriteriosRegra__c emailCriterio = new CriteriosRegra__c();
        emailCriterio.Campo__c      = campo;
        emailCriterio.Operador__c   = operador;
        emailCriterio.Valor__c      =  valor;
        emailCriterio.RegradeEmail__c = regraEmail;

        return emailCriterio;
    }

    /**
    * 
    *
    */
    public static Product2 productInstance( ) {
        Product2 thisProduct = new Product2();
        thisProduct.Name            = 'ProductTeste';
        thisProduct.ProductCode     = 'test123';
        thisProduct.CodigoStored__c = 'codStored123';
        thisProduct.Family          = 'Frontend';
        thisProduct.IsActive        = TRUE;

        return thisProduct;
    }


    /**
    * 
    *
    */
    public static Case casewithcontactInstance( String contactId ) {
        Case thisCase = new Case();
        thiscase.AccountId      = contactId;
        thiscase.SuppliedEmail  = '';
        thiscase.SuppliedName   = 'testeWEB';
        thisCase.Status         = 'Aberto';
        thisCase.Origin         = 'Email - Atendimento';
        thiscase.Type           = 'Vendas';
        thiscase.Subject        = 'Assunto';
        thiscase.Description    = 'Descrição';
        thisCase.RecordTypeId   = Schema.SObjectType.Case.getRecordTypeInfosByName().get( 'Empiricus' ).getRecordTypeId();

        return thisCase;
    }

    /**
    * 
    *
    */
     public static Case caseInstance() {
        Case thisCase = new Case();
        thiscase.SuppliedEmail      = 'teste@teste.com.br';
        thisCase.Subject            = 'Subject';
        thisCase.Status             = 'Novo';
        thisCase.Description        = 'Description';
        thisCase.Origin             = 'Email - Atendimento';
        thiscase.RecordTypeId       = Schema.SObjectType.Case.getRecordTypeInfosByName().get( 'Empiricus' ).getRecordTypeId();

        return thisCase;
    }

    public static HubService__c hubServiceInstance(){
        HubService__c thisHubs = new HubService__c();
        thisHubs.RecordTypeId       = Schema.SObjectType.HubService__c.getRecordTypeInfosByName().get( 'Relacionamento - Empiricus' ).getRecordTypeId();
        thisHubs.Classificacao__c   = 'Reserva';
        thisHubs.Tipo__c            = 'Relacionamento';
        thisHubs.Conteudo__c        = 'Clube Empiricus';

        return thisHubs;
    }
    
     public void fcoverage(){
        String a;        
        
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';    
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';
        a = 'a';     
    }


}