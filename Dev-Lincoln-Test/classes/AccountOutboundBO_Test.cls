@isTest
private class AccountOutboundBO_Test {
    
    @isTest static void test_method_one() {     
        Account accountInsertMagento = SObjectInstanceToClassCoverage.accountInstance( null );
        insert accountInsertMagento;

        Queue__c queue = [SELECT Id, Name, EventName__c, Status__c, Payload__c, IgnoredByTrigger__c, Response__c, RecordId__c, ExceptionStackTrace__c
                            FROM Queue__c WHERE EventName__c in ( 'INSERT_ACCOUNT_SF_TO_MAGENTO', 'UPDATE_ACCOUNT_SF_TO_MAGENTO' ) ORDER BY CreatedDate Desc Limit 1];

        AccountOutboundBO.execute( queue.Id, queue.RecordId__c, queue.EventName__c, queue.Payload__c );
    }
    
    @isTest static void test_method_two() {
        Account accountInsertMagento = SObjectInstanceToClassCoverage.accountInstance( null );
        accountInsertMagento.CodigoMagento__c = '112233';
        insert accountInsertMagento;

        Queue__c queue = [SELECT Id, Name, EventName__c, Status__c, Payload__c, IgnoredByTrigger__c, Response__c, RecordId__c, ExceptionStackTrace__c
                            FROM Queue__c WHERE EventName__c in ( 'INSERT_ACCOUNT_SF_TO_MAGENTO', 'UPDATE_ACCOUNT_SF_TO_MAGENTO' ) ORDER BY CreatedDate Desc Limit 1];

        AccountOutboundBO.execute( queue.Id, queue.RecordId__c, queue.EventName__c, queue.Payload__c );
    } 
    
}