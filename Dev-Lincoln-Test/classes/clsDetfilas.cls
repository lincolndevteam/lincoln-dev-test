public class clsDetfilas {
    private final Group fila;
    public List<string> Objetos {get;set;}
    public List<string> UsersOrGroups {get;set;}
    public string Tipo {get;set;}
    public boolean Pesquisado{get;set;}
    public List<UserorGroupClass> lstUserorGroup{get;set;}
    public String idFilaObjeto{get;set;}
    public String idUsuarioGrupo{get;set;}
    public clsDetfilas (ApexPages.StandardController stdController){
        if(Tipo=='' || Tipo==null){
            Pesquisado=false;
        }
        
        this.fila = (Group)stdController.getRecord();
        getlstUserorGroup();
    }
    public pageReference atualizaPagina (){
        PageReference ret= new PageReference('/apex/detFilas?id=' +Fila.id);
        ret.setRedirect(true);
        return ret;
        
    }
    public pageReference ExcluirObjeto (){
        String idDelete='';
        if(idUsuarioGrupo !='' && idUsuarioGrupo!=null){
            GroupMember objGroup = [Select id from GroupMember where groupId=:Fila.Id and userorgroupid=:idUsuarioGrupo];
            idUsuarioGrupo = objGroup.Id;
            
        }
        idDelete = (idFilaObjeto!='' && idFilaObjeto!=null?idFilaObjeto:(idUsuarioGrupo!=null && idUsuarioGrupo!=''?idUsuarioGrupo:''));
        if(idDelete!='')database.delete(idDelete);
        return atualizaPagina();
    }
    
    public void getlstUserorGroup(){
        lstUserorGroup = new List<UserorGroupClass>(); 
        //Captura os users:
        for(User objUser : [select id,name from user 
                            where 
                            id in (select userorgroupid from groupmember where groupid=:fila.id)]){
                                UserorGroupClass obj = new UserorGroupClass();
                                obj.idFila=fila.id;
                                obj.IdObjeto=objUser.id;
                                obj.Nome=objUser.Name;
                                lstUserorGroup.add(obj);
                            }
        //Captura as Filas:
        for(Group objGroup : [SELECT id, name,type 
                              FRom group where type ='Regular' 
                              and id in (select userorgroupid from groupmember where groupid=:fila.id)]){
                                  UserorGroupClass obj = new UserorGroupClass();
                                obj.idFila=fila.id;
                                obj.IdObjeto=objGroup.id;
                                obj.Nome=objGroup.Name;
                                lstUserorGroup.add(obj);
                              }
        
        
    }
    public pagereference SalvarFila(){
        if(fila.id!=null){
            GerarInformacao();
            database.update(fila); 
        }
        else{
            fila.Type='Queue';
            database.insert(fila);
            System.debug('id da Fila : ' + Fila.Id);
            GerarInformacao();
        }
        return atualizaPagina();
    }
    public  void GerarInformacao(){
        //Recupera os objetos selecionados
        List<QueueSobject> lstQueuObjects = new List<QueueSobject>();
        for(String strObj : Objetos){
            QueueSobject objQueue = new QueueSobject();
            objQueue.QueueId=Fila.id;
            objQueue.SobjectType=strObj;
            lstQueuObjects.add(objQueue);
        }
        
        //recupera os users / grupos selecionados
        List<GroupMember> lstGroupMember = new List<GroupMember>();
        for(String strUserOrGroup : UsersOrGroups){
            GroupMember objGroup = new GroupMember();
            objGroup.groupid=fila.id;
            objGroup.userorgroupid = strUserOrGroup;
            lstGroupMember.add(objGroup);
        }
        if(lstGroupMember.size()>0){database.insert(lstGroupMember);}
        if(lstQueuObjects.size()>0){database.insert(lstQueuObjects);}
        
    }
    public pagereference Voltar(){
        PageReference ref = new PageReference('/apex/ManagerQueue');
        ref.setRedirect(true);
        return ref;
    }
    public List<SelectOption> getTiposDisp()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Selecione'));
        options.add(new SelectOption('U','Usuarios'));
        options.add(new SelectOption('G','Grupos públicos'));
        return options;
        
    }
    public List<SelectOption> getUserOrGroup(){
        List<SelectOption> options = new List<SelectOption>();
        if(Tipo=='U'){
            for(User objUser : [select id,name from user 
                                where isactive=true and 
                                id not in (select userorgroupid from groupmember where groupid=:fila.id)]){
                                    options.add(new SelectOption(objUser.id,objUser.Name));
                                }
            
        }
        else if (tipo=='G'){
            for(Group objGroup : [SELECT id, name,type 
                                  FRom group where type ='Regular' 
                                  and id not in (select userorgroupid from groupmember where groupid=:fila.id)]){
                                      options.add(new SelectOption(objGroup.id,objGroup.Name));
                                  }
            
        }
        if(Tipo!='' && Tipo!=null){
            Pesquisado=true;
        }
        else{
            Pesquisado=false; 
        }
        return Options;
    }
    public List<SelectOption> getsObjects()
    {
        List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();     
        List<SelectOption> options = new List<SelectOption>();
        Map<String,String> mapQueu= new  Map<String,string>();
        for(QueueSobject obj : [select SobjectType,Queueid 
                                from QueueSobject where Queueid=:Fila.Id]){
                                    mapQueu.put(obj.SobjectType,obj.SobjectType);
                                }
        
        System.debug('O mapa e:' + mapQueu);
        for(Schema.SObjectType f : gd)
        {
            if(f.getDescribe().isCustom() || 
               (!f.getDescribe().isCustomSetting() && 
                (f.getDescribe().getName() =='Case'  || 
                 f.getDescribe().getName() =='Lead' ||
                 f.getDescribe().getName() =='ServiceContract') && mapQueu.get(f.getDescribe().getName())==null )){
                     options.add(new SelectOption(f.getDescribe().getName(),f.getDescribe().getLabel()));
                 }
            
            
            
            
            
        }
        return options;
    }
    public class UserorGroupClass{
        public String IdObjeto{get;set;}
        public String idFila{get;set;}
        public  String Nome{get;set;}
        public UserorGroupClass(){}
        
    }
    
}