/**
*	@author Diego Moreira
*	@class Classe de integração com o magento
*/
public class MagentoOutboundIS {
	/*
        Singleton
    */
    private static final MagentoOutboundIS instance = new MagentoOutboundIS();    
    private MagentoOutboundIS(){}
    
    public static MagentoOutboundIS getInstance() {
        return instance;
    }

    /**
	*	Fazer login na base do magento
	*	@param payload XML com os dados de integração
	*/
	public String doPost( String payload ) {
		HttpRequest req = new HttpRequest();
		req.setEndpoint( Setup__c.getValues( 'MAGENTO_ENDPOINT' ).Value__c );
		req.setMethod( 'POST' );
		req.setHeader( 'content-type', 'text/xml;charset=UTF-8' );
		req.setTimeout( 120000 );
		req.setBody( payload );		 
		
		Http h = new Http();
		if( Test.isRunningTest() ) return MagentoOutboundUtil.getXMLCustomerResponse_Test();
		HttpResponse res = h.send( req );

		return res.getBody();
	}
}