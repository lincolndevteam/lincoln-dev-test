public class PlanoOfertaDAO {

        /*
        Singleton
    */
    private static final PlanoOfertaDAO instance = new PlanoOfertaDAO();    
    private PlanoOfertaDAO(){}
    
    public static PlanoOfertaDAO getInstance() {
        return instance; 
    }
    
    public List<PlanoOferta__c> getPlansOffer( Set<String> offerList ) {
        return [SELECT Id, Plano__c, Oferta__c
                FROM PlanoOferta__c
                WHERE Oferta__c in :offerList];
    }
    
}