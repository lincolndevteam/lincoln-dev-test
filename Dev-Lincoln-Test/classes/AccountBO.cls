/**
*    @author Diego Moreira
*    @class Classe de negocio de Conta
*/
public with sharing class AccountBO {
    /*
        Singleton
    */
    private static final AccountBO instance = new AccountBO();    
    private AccountBO(){}
    
    public static AccountBO getInstance() {
        return instance;
    }

    /**
    *   Cria fila de integração 
    *   @param accounts Contas criadas ou atualizadas
    */
    public void criaFilaIntegracao( List<Account> accounts ) {
        List<String> accountIds = new List<String>();
        List<Queue__c> queueToInsert = new List<Queue__c>();

        for( Account account : accounts ) {
            accountIds.add( account.Id );
        }
       
        if( !accountIds.isEmpty() ) {
            for( Account account : AccountDAO.getInstance().getAccountsById( accountIds ) ) {
                if( account.RecordType.DeveloperName.equals( 'BR_Jolivi' ) ) {
                    String xmlResult;
                    if( Util.getString( account.CodigoMagento__c ).equals( '' ) ) {
                        xmlResult = AccountOutboundBO.createRequest( account );
                        queueToInsert.add( QueueBO.getInstance().sObjectQueue( account.Id, QueueEventNames.INSERT_ACCOUNT_SF_TO_MAGENTO.name(), xmlResult, false ) );                
                    } else {
                        xmlResult = AccountOutboundBO.updateRequest( account );
                        queueToInsert.add( QueueBO.getInstance().sObjectQueue( account.Id, QueueEventNames.UPDATE_ACCOUNT_SF_TO_MAGENTO.name(), xmlResult, false ) );
                    }
                }
            }
            insert queueToInsert;           
        }
    }

    /**
    *   Metodo de criação / atualização de conta
    *   @param payload JSON com as informações 
    */
    public void upsertAccountWStore( String payload ) {
        List<Account> accountToInsert = new List<Account>();        
        Map<String, Object> mapResult = ( Map<String, Object> ) JSON.deserializeUntyped( payload );

        for( Object objResult : ( List<Object> ) mapResult.get( 'objects' ) ) {
            Map<String, Object> mapAccount = ( Map<String, Object> ) objResult;
            
            Account account             = new Account();
            account.CodigoMagento__c      = Util.getString( mapAccount.get( 'id' ) );
            account.TotalGasto__c       = Util.getDecimal( Util.getString(mapAccount.get( 'total_spent' )) );
            String cpf_cnpj             = Util.getString( mapAccount.get( 'cpf_cnpj' ) );
            account.Type                = cpf_cnpj.length() == 14 ? 'Pessoa Física' : 'Pessoa Jurídica';
            account.CPF__c              = cpf_cnpj.length() == 14 ? cpf_cnpj.replace('.', '').replace('-','') : '';
            account.CNPJ__c             = cpf_cnpj.length() == 18 ? cpf_cnpj : '';
            account.Phone               = Util.getString( mapAccount.get( 'phone' ) );
            account.FirstName           = Util.getFirstName(Util.getString( mapAccount.get( 'name' ) ));
            account.LastName            = Util.getLastName(Util.getString( mapAccount.get( 'name' ) ));
            account.TotalPlanos__c      = Util.getDecimal(Util.getString( mapAccount.get( 'subscriptions_total' ) ));
            account.PersonEmail         = Util.getString( mapAccount.get( 'email' ) );
            account.PersonMobilePhone   = Util.getString( mapAccount.get( 'mobile' ) );
            account.PersonBirthdate     = Util.getObjectToDate( mapAccount.get( 'date_birthday') );
    
            accountToInsert.add( account );
        }
        try{
            upsert accountToInsert Account.CodigoMagento__c;
        } catch(DmlException ex){
            System.debug('upsertAccountWStore >>>' + ex.getMessage());
        }
    }

    /**
    *   Metodo para callout para obter informaçòes do cliente na Store
    *   @param param Variavel contendo o email ou codigo store
    */    
    public void updateAccountWStore( String recordId, String result ) {
        Boolean isValidEmail = Util.validarEmail( result );
        
        if( !result.equals('')) {
            Map<String,Object> response = new Map<String,Object>();
            if( isValidEmail ) {
                response = StoreOutboundIS.getInstance().getStoreCustomerByEmail(result);
                AccountInboundBO.updateAccountWStoreArray( recordId, JSON.serialize( response ) );
            } else {
                result = result.replace( 'E-', '' );
                response = StoreOutboundIS.getInstance().getStoreCustomerById(result); 
                AccountInboundBO.updateAccountWStore( recordId, JSON.serialize( response ) );  
            }  
        }
    }
}