/**
*	@author Diego Moreira
*	@class Classe de serviço integração Account
*/
@RestResource ( urlMapping='/v1/account/*' )
global class AccountInboundIS {
	/**
	*	Serviço de recebimento de criação
	*/
	@HttpPost
	global static String doPost() {  
        String jsonResult = RestContext.request.requestBody.toString();

		String queueId 	= QueueBO.getInstance().insertQueue( QueueEventNames.INSERT_ACCOUNT_MAGENTO_TO_SF.name(), jsonResult, false );
        Queue__c result = QueueDAO.getInstance().getQueueById( queueId )[0];        

		return result.Response__c;   		
	}

	/**
    *    Serviço de recebimento de atualização
    */
    @HttpPut 
    global static String doPut() {
        String jsonResult = RestContext.request.requestBody.toString();

		String queueId 	= QueueBO.getInstance().insertQueue( QueueEventNames.UPDATE_ACCOUNT_MAGENTO_TO_SF.name(), jsonResult, false );
        Queue__c result = QueueDAO.getInstance().getQueueById( queueId )[0];
        
        return result.Response__c;      
    }
}