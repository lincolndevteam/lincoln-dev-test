public class OfertaPedidoDAO {

    /*
        Singleton
    */
    private static final OfertaPedidoDAO instance = new OfertaPedidoDAO();    
    private OfertaPedidoDAO(){}
    
    public static OfertaPedidoDAO getInstance() {
        return instance;
    } 
    
    /*
    public List<OfertaPedido__c> getOfferOrder( List<String> orderId ) {
        return [SELECT Id
                FROM OfertaPedido__c
                WHERE Pedido__c in :orderId];
    }
    */
}