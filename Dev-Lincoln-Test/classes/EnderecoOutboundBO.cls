/**
*   @author Diego Moreira
*   @class Classe de negocio da integração de endereço
*/
public class EnderecoOutboundBO implements IQueueProcessing {
	/** 
    *   Processamento da fila 
    *	@param queueId Id da fila de processamento 
    *	@param recordId Id de registro	
    *   @param eventName nome do evento de processamento
    *   @param payload JSON com o item da fila para processamento
    */     
    public static void execute( String queueId, String recordId, String eventName, String payload ) {
    	syncToServer( queueId, recordId, eventName, payload );
    }

    @future( Callout=true )
    @TestVisible private static void syncToServer( String queueId, String recordId, String eventName, String payload ) {
    	try {
            String loginResult; 
            if( Test.isRunningTest() ) loginResult = MagentoOutboundUtil.getXMLLoginResponse_Test();
            else loginResult = MagentoOutboundIS.getInstance().doPost( MagentoOutboundUtil.getXMLLogin() );
        
            DOM.Document doc = new DOM.Document();
            doc.load( loginResult );                
            DOM.XMLNode xmlNode = doc.getRootElement();

            String token = Util.readXMLTag( xmlNode, 'loginReturn').trim();
            String serviceError = Util.readXMLError( xmlNode );

            if( String.isNotBlank( token ) ) {
                payload = String.format( payload, new String[]{ token } );

                if( eventName.equals( QueueEventNames.INSERT_ADDRESS_SF_TO_MAGENTO.name() ) ) {
                	createAddressMagento( queueId, recordId, payload );               
                } else if( eventName.equals( QueueEventNames.UPDATE_ADDRESS_SF_TO_MAGENTO.name() ) ) {
                   	updateAddressMagento( queueId, payload );   
                } 
            } else {
                QueueBO.getInstance().updateQueue( queueId, loginResult, serviceError );
            }                                                         
        } catch( CalloutException ex ) {
            QueueBO.getInstance().updateQueue( queueId, eventName + ' / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
        } catch( Exception ex ) {
            QueueBO.getInstance().updateQueue( queueId, eventName + ' / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
        }  
    }

    /**
    *   Envia os dados de criação de endereço para o Magento
    *   @param queueId Id da fila de processamento
    *   @param recordId Id do registro que gerou a transação
    *   @param payload XML com os dados de transação
    */
    private static void createAddressMagento( String queueId, String recordId, String payload ) {
        String response = MagentoOutboundIS.getInstance().doPost( payload );

        DOM.Document doc = new DOM.Document();
        doc.load( response );                
        DOM.XMLNode xmlNode = doc.getRootElement();

        String magentoCode = Util.readXMLTag( xmlNode, 'result' );
        String serviceError = Util.readXMLError( xmlNode );

        if( String.isNotBlank( magentoCode ) ) {
            ProcessControl.inFutureContext = true;
            update new Endereco__c( Id = recordId, CodigoMagento__c = magentoCode );
        }

        QueueBO.getInstance().updateQueue( queueId, response, serviceError );
    }

    /**
    *   Envia os dados de atualização de endereço para o Magento
    *   @param queueId Id da fila de processamento
    *   @param payload XML com os dados de transação
    */
    private static void updateAddressMagento( String queueId, String payload ) {
    	String response = MagentoOutboundIS.getInstance().doPost( payload );

        DOM.Document doc = new DOM.Document();
        doc.load( response );                
        DOM.XMLNode xmlNode = doc.getRootElement();

        String serviceError = Util.readXMLError( xmlNode );        
        QueueBO.getInstance().updateQueue( queueId, response, serviceError );
    }

    /**
	*	Cria JSON com os dados do objeto
	*	@param account objeto de contas para integração
    */
    public static String createEnderecoRequest( Endereco__c endereco ) {
        String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
        String xsi = 'http://www.w3.org/2001/XMLSchema-instance';
        String xsd = 'http://www.w3.org/2001/XMLSchema';
        String urn = 'urn:Magento';

        DOM.Document doc = new DOM.Document();
        dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');
        envelope.setNamespace( 'xsi', xsi );
        envelope.setNamespace( 'xsd', xsd );
        envelope.setNamespace( 'urn', urn );

        dom.XmlNode header = envelope.addChildElement( 'Header', soapNS, null );
        dom.XmlNode body = envelope.addChildElement( 'Body', soapNS, null );
        dom.XmlNode customer = body.addChildElement( 'customerAddressCreate', urn, null );  
        dom.XmlNode sessionId = customer.addChildElement( 'sessionId', null, null );
        sessionId.addTextNode( '{0}' );
        dom.XmlNode customerId = customer.addChildElement( 'customerId', null, null );
        customerId.addTextNode( Util.getString( endereco.Conta__r.CodigoMagento__c ) );
        dom.XmlNode data = customer.addChildElement( 'addressData', null, null );
        dom.XmlNode city = data.addChildElement( 'city', null, null );
        city.addTextNode( Util.getString( endereco.Cidade__c ) );
        dom.XmlNode company = data.addChildElement( 'company', null, null );
        company.addTextNode( '' );
        dom.XmlNode country_id = data.addChildElement( 'country_id', null, null );
        country_id.addTextNode( Util.getString( endereco.Pais__c ) );
        dom.XmlNode fax = data.addChildElement( 'fax', null, null );
        fax.addTextNode( '' );
        dom.XmlNode firstname = data.addChildElement( 'firstname', null, null );
        firstname.addTextNode( Util.getString( endereco.Conta__r.FirstName ) );
        dom.XmlNode lastname = data.addChildElement( 'lastname', null, null );
        lastname.addTextNode( Util.getString( endereco.Conta__r.LastName ) );
        dom.XmlNode middlename = data.addChildElement( 'middlename', null, null );
        middlename.addTextNode( '' );
        dom.XmlNode postcode = data.addChildElement( 'postcode', null, null );
        postcode.addTextNode( Util.getString( endereco.Cep__c ) );
        dom.XmlNode prefix = data.addChildElement( 'prefix', null, null );
        prefix.addTextNode( '' );
        dom.XmlNode region_id = data.addChildElement( 'region_id', null, null );
        region_id.addTextNode( '' );
        dom.XmlNode region = data.addChildElement( 'region', null, null );
        region.addTextNode( Util.getString( endereco.Estado__c ) );
        dom.XmlNode street = data.addChildElement( 'street', null, null );
        street.addTextNode( Util.getString( endereco.Name ) + ' ' + Util.getString( endereco.Numero__c ) + ' ' + Util.getString( endereco.Complemento__c ) + ' ' + Util.getString( endereco.Bairro__c )  );
        dom.XmlNode suffix = data.addChildElement( 'suffix', null, null );
        suffix.addTextNode( '' );
        dom.XmlNode telephone = data.addChildElement( 'telephone', null, null );
        telephone.addTextNode( Util.getString( endereco.Telefone__c ) );
        dom.XmlNode is_default_billing = data.addChildElement( 'is_default_billing', null, null );
        is_default_billing.addTextNode( Util.getString( Util.getBoolean( endereco.PadraoCobranca__c ) ) );
        dom.XmlNode is_default_shipping = data.addChildElement( 'is_default_shipping', null, null );
        is_default_shipping.addTextNode( Util.getString( Util.getBoolean( endereco.PadraoEntrega__c ) ) );
        dom.XmlNode sfid = data.addChildElement( 'sfid', null, null );
        sfid.addTextNode( endereco.Id );

        return doc.toXmlString();
    }

    /**
    *   Cria JSON com os dados do objeto
    *   @param account objeto de contas para integração
    */
    public static String updateEnderecoRequest( Endereco__c endereco ) {
        String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
        String xsi = 'http://www.w3.org/2001/XMLSchema-instance';
        String xsd = 'http://www.w3.org/2001/XMLSchema';
        String urn = 'urn:Magento';

        DOM.Document doc = new DOM.Document();
        dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');
        envelope.setNamespace( 'xsi', xsi );
        envelope.setNamespace( 'xsd', xsd );
        envelope.setNamespace( 'urn', urn );

        dom.XmlNode header = envelope.addChildElement( 'Header', soapNS, null );
        dom.XmlNode body = envelope.addChildElement( 'Body', soapNS, null );
        dom.XmlNode customer = body.addChildElement( 'customerAddressUpdate', urn, null );
        dom.XmlNode sessionId = customer.addChildElement( 'sessionId', null, null );
        sessionId.addTextNode( '{0}' );
        dom.XmlNode addressId = customer.addChildElement( 'addressId', null, null );
        addressId.addTextNode( Util.getString( endereco.CodigoMagento__c ) );
        dom.XmlNode data = customer.addChildElement( 'addressData', null, null );
        dom.XmlNode city = data.addChildElement( 'city', null, null );
        city.addTextNode( Util.getString( endereco.Cidade__c ) );
        dom.XmlNode company = data.addChildElement( 'company', null, null );
        company.addTextNode( '' );
        dom.XmlNode country_id = data.addChildElement( 'country_id', null, null );
        country_id.addTextNode( Util.getString( endereco.Pais__c ) );
        dom.XmlNode fax = data.addChildElement( 'fax', null, null );
        fax.addTextNode( '' );
        dom.XmlNode firstname = data.addChildElement( 'firstname', null, null );
        firstname.addTextNode( Util.getString( endereco.Conta__r.FirstName ) );
        dom.XmlNode lastname = data.addChildElement( 'lastname', null, null );
        lastname.addTextNode( Util.getString( endereco.Conta__r.LastName ) );
        dom.XmlNode middlename = data.addChildElement( 'middlename', null, null );
        middlename.addTextNode( '' );
        dom.XmlNode postcode = data.addChildElement( 'postcode', null, null );
        postcode.addTextNode( Util.getString( endereco.Cep__c ) );
        dom.XmlNode prefix = data.addChildElement( 'prefix', null, null );
        prefix.addTextNode( '' );
        dom.XmlNode region_id = data.addChildElement( 'region_id', null, null );
        region_id.addTextNode( '' );
        dom.XmlNode region = data.addChildElement( 'region', null, null );
        region.addTextNode( Util.getString( endereco.Estado__c ) );
        dom.XmlNode street = data.addChildElement( 'street', null, null );
        street.addTextNode( Util.getString( endereco.Name ) + ' ' + Util.getString( endereco.Numero__c ) + ' ' + Util.getString( endereco.Complemento__c ) + ' ' + Util.getString( endereco.Bairro__c )  );
        dom.XmlNode suffix = data.addChildElement( 'suffix', null, null );
        suffix.addTextNode( '' );
        dom.XmlNode telephone = data.addChildElement( 'telephone', null, null );
        telephone.addTextNode( Util.getString( endereco.Telefone__c ) );
        dom.XmlNode is_default_billing = data.addChildElement( 'is_default_billing', null, null );
        is_default_billing.addTextNode( Util.getString( Util.getBoolean( endereco.PadraoCobranca__c ) ) );
        dom.XmlNode is_default_shipping = data.addChildElement( 'is_default_shipping', null, null );
        is_default_shipping.addTextNode( Util.getString( Util.getBoolean( endereco.PadraoEntrega__c ) ) );
        dom.XmlNode sfid = data.addChildElement( 'sfid', null, null );
        sfid.addTextNode( endereco.Id );

        return doc.toXmlString();
    }
}