@isTest
private class EnderecoOutboundBO_Test {
    
    @isTest static void test_method_one() {
        Account accountInsertMagento = SObjectInstanceToClassCoverage.accountInstance( null );
        accountInsertMagento.CodigoMagento__c = '112233';
        insert accountInsertMagento;

        Endereco__c enderecoInsertMagento = SObjectInstanceToClassCoverage.enderecoInstance( null, accountInsertMagento.Id );
        insert enderecoInsertMagento;

        Queue__c queue = [SELECT Id, Name, EventName__c, Status__c, Payload__c, IgnoredByTrigger__c, Response__c, RecordId__c, ExceptionStackTrace__c
                            FROM Queue__c WHERE EventName__c in ( 'INSERT_ADDRESS_SF_TO_MAGENTO', 'UPDATE_ADDRESS_SF_TO_MAGENTO' ) ORDER BY CreatedDate Desc Limit 1]; // WHERE RecordId__c = :enderecoInsertMagento.Id

        EnderecoOutboundBO.execute( queue.Id, queue.RecordId__c, queue.EventName__c, queue.Payload__c );
    } 
    
    @isTest static void test_method_two() {
        Account accountInsertMagento = SObjectInstanceToClassCoverage.accountInstance( null );
        accountInsertMagento.CodigoMagento__c = '112233';
        insert accountInsertMagento;

        Endereco__c enderecoInsertMagento = SObjectInstanceToClassCoverage.enderecoInstance( null, accountInsertMagento.Id );
        enderecoInsertMagento.CodigoMagento__c = '112233';
        insert enderecoInsertMagento;

        Queue__c queue = [SELECT Id, Name, EventName__c, Status__c, Payload__c, IgnoredByTrigger__c, Response__c, RecordId__c, ExceptionStackTrace__c
                            FROM Queue__c WHERE EventName__c in ( 'INSERT_ADDRESS_SF_TO_MAGENTO', 'UPDATE_ADDRESS_SF_TO_MAGENTO' ) ORDER BY CreatedDate Desc Limit 1]; // WHERE RecordId__c = :enderecoInsertMagento.Id

        EnderecoOutboundBO.execute( queue.Id, queue.RecordId__c, queue.EventName__c, queue.Payload__c );
        Util.jsonGenericResponse(queue.Payload__c);
    }
    
}