/**
*   @author Diego Moreira
*   @class Classe DAO do objeto tipo de registro
*/
public with sharing class RecordTypeDAO {
    /*
        Singleton
    */  
    private static final RecordTypeDAO instance = new RecordTypeDAO();    
    private RecordTypeDAO(){}
    
    public static RecordTypeDAO getInstance() {
        return instance;
    }

    /**
    *   Retorna a lista de tipo de registro do objeto
    */
    public List<RecordType> getRecordTypeByName( List<String> name, String sobjectType ){
        return [SELECT Id, Name, DeveloperName 
            FROM RecordType 
            WHERE Name in :name 
            AND SobjectType = :sobjectType];
    }

    /**
    *   Retorna a lista de tipo de registro do objeto
    */
    public List<RecordType> getRecordTypeByDeveloperName( String sobjectType ){
        return [SELECT Id, Name, DeveloperName 
                    FROM RecordType 
                    WHERE SobjectType = :sobjectType
                    AND IsActive = true];
    }
}