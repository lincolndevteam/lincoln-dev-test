/**
*    @author Diego Moreira
*    @interface interface responsavel pelo processamento da fila
*/
public interface IQueueProcessing {
	void execute( String queueId, String recordId, String eventName, String payload );
}