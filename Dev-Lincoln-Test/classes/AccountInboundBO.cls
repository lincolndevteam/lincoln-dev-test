/**
*   @author Diego Moreira
*   @class Classe de negocio da integração de contas
*/
public class AccountInboundBO implements IQueueProcessing {
    /** 
    *   Processamento da fila 
    *   @param queueId Id da fila de processamento 
    *   @param recordId Id de registro  
    *   @param eventName nome do evento de processamento
    *   @param payload JSON com o item da fila para processamento
    */     
    public static void execute( String queueId, String recordId, String eventName, String payload ) {
        //Savepoint sp = Database.setSavepoint();
        Database.SaveResult[] dmlResult;
        Database.UpsertResult[] upsertResult; 

        try {
            if( eventName.equals( QueueEventNames.INSERT_ACCOUNT_MAGENTO_TO_SF.name() ) ) {
                dmlResult = insertAccount( payload );       
            } else if( eventName.equals( QueueEventNames.UPDATE_ACCOUNT_MAGENTO_TO_SF.name() ) ) {
                dmlResult = updateAccount( payload ); 
            }else if( eventName.equals( QueueEventNames.QUERY_ACCOUNT_STORE_TO_SALESFORCE.name() ) ) {
                upsertResult = updateAccountWStore(payload);
                system.debug('upsertResult >>>' + upsertResult.size());
                CaseBO.getInstance().setCustomerInCase(recordId, upsertResult.size() == 0 ? false : upsertResult[0].isSuccess());
            }

            if(dmlResult != null)
                QueueBO.getInstance().updateQueue( queueId, response( dmlResult ), '' );

            if(upsertResult != null){
                QueueBO.getInstance().updateQueue( queueId, upsertResult );

            }

        } catch( Exception ex ) {
            //Database.rollback(sp);
            QueueBO.getInstance().updateQueue( queueId, Util.jsonGenericResponse( ex.getMessage() ), eventName + ' / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
        }
    }

    /**
    *   Metodo de criação de conta
    *   @param payload JSON com as informações 
    */
    private static Database.SaveResult[] insertAccount( String payload ) {
        List<Account> accountToInsert = new List<Account>();        
        Map<String, Object> mapResult = ( Map<String, Object> ) JSON.deserializeUntyped( payload );
        Map<String, String> mapRecordType = mapRecordType( mapResult );

        for( Object objResult : ( List<Object> ) mapResult.get( 'account' ) ) {
            Map<String, Object> mapAccount = ( Map<String, Object> ) objResult;
            Account account = new Account();
            account.RazaoSocial__c              = Util.getString( mapAccount.get( 'razaosocial' ) );
            account.FirstName                   = Util.getString( mapAccount.get( 'nome' ) );
            account.LastName                    = Util.getString( mapAccount.get( 'sobrenome' ) );
            account.TradingName__c              = Util.getString( mapAccount.get( 'nomefantasia' ) );            
            account.NomeResponsavel__c          = Util.getString( mapAccount.get( 'nomeresponsavel' ) );
            account.CPF__c                      = Util.getString( mapAccount.get( 'cpf' ) );
            account.Celular__c                  = Util.getString( mapAccount.get( 'celular' ) );
            account.Email__c                    = Util.getString( mapAccount.get( 'email' ) );               
            account.InscricaoEstadual__c        = Util.getString( mapAccount.get( 'inscricaoestadual' ) );
            account.CNPJ__c                     = Util.getString( mapAccount.get( 'cnpj' ) );                       
            account.Phone                       = Util.getString( mapAccount.get( 'telefone' ) );                 
            account.DesejaReceberNewsletter__c  = Util.getBoolean( mapAccount.get( 'newsletter' ) );
            account.DesejaReceberSMS__c         = Util.getBoolean( mapAccount.get( 'sms' ) );
            account.Servico__c                  = Util.getString( mapAccount.get( 'servico' ) );            
            account.CodigoMagento__c            = Util.getString( mapAccount.get( 'codigomagento' ) );
            account.Type                        = Util.getString( mapAccount.get( 'tipo' ) );
            account.DataNascimento__c           = Util.getObjectToDate( mapAccount.get( 'datanascimento' ) );
            if( mapRecordType.get( Util.getString( mapAccount.get( 'tiporegistro' ) ) ) != null ) 
                account.RecordTypeId = mapRecordType.get( Util.getString( mapAccount.get( 'tiporegistro' ) ) );

            accountToInsert.add( account );
        }

        Database.SaveResult[] result = Database.insert( accountToInsert, false );
        return result;
    }

    /**
    *   Metodo de atualização de conta
    *   @param payload JSON com as informações 
    */
    private static Database.SaveResult[] updateAccount( String payload ) {
        List<Account> accountToUpdate = new List<Account>();
        Map<String, Object> mapResult = ( Map<String, Object> ) JSON.deserializeUntyped( payload );
        Map<String, String> mapRecordType = mapRecordType( mapResult );

        for( Object objResult : ( List<Object> ) mapResult.get( 'account' ) ) {
            Map<String, Object> mapAccount = ( Map<String, Object> ) objResult;
            Account account = new Account(); 
            account.Id                          = Util.getString( mapAccount.get( 'sfid' ) );           
            account.RazaoSocial__c              = Util.getString( mapAccount.get( 'razaosocial' ) );
            account.FirstName                   = Util.getString( mapAccount.get( 'nome' ) );
            account.LastName                    = Util.getString( mapAccount.get( 'sobrenome' ) );
            account.TradingName__c              = Util.getString( mapAccount.get( 'nomefantasia' ) );            
            account.NomeResponsavel__c          = Util.getString( mapAccount.get( 'nomeresponsavel' ) );
            account.CPF__c                      = Util.getString( mapAccount.get( 'cpf' ) );
            account.DataNascimento__c           = Util.getObjectToDate( mapAccount.get( 'datanascimento' ) );
            account.Celular__c                  = Util.getString( mapAccount.get( 'celular' ) );
            account.Email__c                    = Util.getString( mapAccount.get( 'email' ) );               
            account.InscricaoEstadual__c        = Util.getString( mapAccount.get( 'inscricaoestadual' ) );
            account.CNPJ__c                     = Util.getString( mapAccount.get( 'cnpj' ) );                       
            account.Phone                       = Util.getString( mapAccount.get( 'telefone' ) );                 
            account.DesejaReceberNewsletter__c  = Util.getBoolean( mapAccount.get( 'newsletter' ) );
            account.DesejaReceberSMS__c         = Util.getBoolean( mapAccount.get( 'sms' ) );
            account.Servico__c                  = Util.getString( mapAccount.get( 'servico' ) );            
            account.CodigoMagento__c            = Util.getString( mapAccount.get( 'codigomagento' ) );
            account.Type                        = Util.getString( mapAccount.get( 'tipo' ) );
            if( mapRecordType.get( Util.getString( mapAccount.get( 'tiporegistro' ) ) ) != null ) 
                account.RecordTypeId = mapRecordType.get( Util.getString( mapAccount.get( 'tiporegistro' ) ) );

            accountToUpdate.add( account );
        }

        Database.SaveResult[] result = Database.update( accountToUpdate, false );
        return result;
    }

    /**
    *   Metodo de criação / atualização de conta
    *   @param payload JSON com as informações 
    */
    public static void updateAccountWStore( String recordId, String payload ) {        
        List<Account> accountToUpdate = new List<Account>(); 
        List<Assinatura__c> subscriptionToUpdate = new List<Assinatura__c>(); 
        List<Assinatura__c> orderToUpdate = new List<Assinatura__c>();        
        Map<String, Object> mapAccount = ( Map<String, Object> ) JSON.deserializeUntyped( payload );
        
        accountToUpdate.add( mapAccountStore( recordId, mapAccount ) );
        String codigoStore = 'E-' + Util.getString( mapAccount.get( 'id' ) );
        
        for(Object objSubscriptionsResult : ( List<Object> ) mapAccount.get( 'subscriptions' ) ){    
            Map<String, Object> mapSubscriptions        = ( Map<String, Object> ) objSubscriptionsResult;

            subscriptionToUpdate.add( mapSubscriptionStore( codigoStore, mapSubscriptions ) );
        }
        
        for(Object objOrderResult : ( List<Object> ) mapAccount.get( 'orders' ) ){ 
            Map<String, Object> mapOrders = ( Map<String, Object> ) objOrderResult;   
            
            orderToUpdate.add( mapOrderStore( codigoStore, mapOrders ) );
        }        
        
        update accountToUpdate;
        Database.UpsertResult[] subscriptionResult = Database.upsert( subscriptionToUpdate, Assinatura__c.CodigoAssinaturaStore__c, true );
        Database.UpsertResult[] orderResult = Database.upsert( orderToUpdate, Assinatura__c.CodigoAssinaturaStore__c, true );
    }

    /**
    *   Metodo de criação / atualização de conta
    *   @param payload JSON com as informações 
    */
    public static void updateAccountWStoreArray( String recordId, String payload ) {        
        List<Account> accountToUpdate = new List<Account>(); 
        List<Assinatura__c> subscriptionToUpdate = new List<Assinatura__c>(); 
        List<Assinatura__c> orderToUpdate = new List<Assinatura__c>();        
        Map<String, Object> mapResult = ( Map<String, Object> ) JSON.deserializeUntyped( payload );

        for( Object objAccountResult : ( List<Object> ) mapResult.get( 'objects' ) ) {
            Map<String, Object> mapAccount = ( Map<String, Object> ) objAccountResult;
            
            accountToUpdate.add( mapAccountStore( recordId, mapAccount ) );
            String codigoStore = 'E-' + Util.getString( mapAccount.get( 'id' ) );
            
            for(Object objSubscriptionsResult : ( List<Object> ) mapAccount.get( 'subscriptions' ) ){    
                Map<String, Object> mapSubscriptions        = ( Map<String, Object> ) objSubscriptionsResult;

                subscriptionToUpdate.add( mapSubscriptionStore( codigoStore, mapSubscriptions ) );
            }
            
            for(Object objOrderResult : ( List<Object> ) mapAccount.get( 'orders' ) ){ 
                Map<String, Object> mapOrders = ( Map<String, Object> ) objOrderResult;   
                
                orderToUpdate.add( mapOrderStore( codigoStore, mapOrders ) );
            }

        }
        
        update accountToUpdate;
        Database.UpsertResult[] subscriptionResult = Database.upsert( subscriptionToUpdate, Assinatura__c.CodigoAssinaturaStore__c, true );
        Database.UpsertResult[] orderResult = Database.upsert( orderToUpdate, Assinatura__c.CodigoAssinaturaStore__c, true );
    }

    /**
    *   Metodo de criação / atualização de conta
    *   @param payload JSON com as informações 
    */
    public static Database.UpsertResult[] updateAccountWStore( String payload ) {        
        List<Account> accountToUpdate = new List<Account>(); 
        List<Assinatura__c> subscriptionToUpdate = new List<Assinatura__c>(); 
        List<Assinatura__c> orderToUpdate = new List<Assinatura__c>();        
        Map<String, Object> mapResult = ( Map<String, Object> ) JSON.deserializeUntyped( payload );

        for( Object objAccountResult : ( List<Object> ) mapResult.get( 'objects' ) ) {
            Map<String, Object> mapAccount = ( Map<String, Object> ) objAccountResult;
            
            accountToUpdate.add( mapAccountStore( '', mapAccount ) );
            String codigoStore = 'E-' + Util.getString( mapAccount.get( 'id' ) );
            
            for(Object objSubscriptionsResult : ( List<Object> ) mapAccount.get( 'subscriptions' ) ){    
                Map<String, Object> mapSubscriptions        = ( Map<String, Object> ) objSubscriptionsResult;
                 
                subscriptionToUpdate.add( mapSubscriptionStore( codigoStore, mapSubscriptions ) );
            }
            
            for(Object objOrderResult : ( List<Object> ) mapAccount.get( 'orders' ) ){ 
                Map<String, Object> mapOrders = ( Map<String, Object> ) objOrderResult;   
                
                orderToUpdate.add( mapOrderStore( codigoStore, mapOrders ) );                
            }           
        }
        
        Database.UpsertResult[] accountResult = Database.upsert( accountToUpdate, Account.CodigoMagento__c, true );
        Database.UpsertResult[] subscriptionResult = Database.upsert( subscriptionToUpdate, Assinatura__c.CodigoAssinaturaStore__c, true );
        Database.UpsertResult[] orderResult = Database.upsert( orderToUpdate, Assinatura__c.CodigoAssinaturaStore__c, true );
        return accountResult;
    }    

    /**
    *
    *
    */
    private static Account mapAccountStore( String accountId, Map<String, Object> mapAccount ) {
        String cpf_cnpj             = Util.getString( mapAccount.get( 'cpf_cnpj' ) );

        Account account             = new Account();
        if( accountId != '' )       account.Id           = accountId;
        account.RecordTypeId        = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Empiricus').getRecordTypeId();
        account.CodigoMagento__c    = 'E-' + Util.getString( mapAccount.get( 'id' ) );
        account.TotalGasto__c       = Util.getDecimal( Util.getString(mapAccount.get( 'total_spent' )) );
        account.Type                = cpf_cnpj.length() == 14 ? 'Pessoa Física' : 'Pessoa Jurídica';
        account.CPF__c              = cpf_cnpj.length() == 14 ? cpf_cnpj.replace('.', '').replace('-','') : '';
        account.CNPJ__c             = cpf_cnpj.length() == 18 ? cpf_cnpj : '';
        account.Phone               = Util.getString( mapAccount.get( 'phone' ) );
        account.FirstName           = Util.getFirstName(Util.getString( mapAccount.get( 'name' ) ));
        account.LastName            = Util.getLastName(Util.getString( mapAccount.get( 'name' ) ));
        account.TotalPlanos__c      = Util.getDecimal(Util.getString( mapAccount.get( 'subscriptions_total' ) ));
        account.PersonEmail         = Util.getString( mapAccount.get( 'email' ) );
        account.PersonMobilePhone   = Util.getString( mapAccount.get( 'mobile' ) );
        account.PersonBirthdate     = Util.getObjectToDate( mapAccount.get( 'date_birthday') );

        return account;
    }

    /**
    *
    *
    */
    private static Assinatura__c mapSubscriptionStore( String codigoStore, Map<String, Object> mapSubscriptions ) {
        Assinatura__c subscription                  = new Assinatura__c();
        subscription.Tipo__c                        = 'Assinatura';
        subscription.CodigoAssinaturaStore__c       = Util.getString(mapSubscriptions.get('id'));
        subscription.Ativo__c                       = Util.getString(mapSubscriptions.get('status')) == 'ATIVA' ? True : False;
        subscription.Status__c                      = Util.getString(mapSubscriptions.get('status'));
        subscription.Cliente__r                     = new Account(CodigoMagento__c = codigoStore);
        subscription.DataAssinaturaPedido__c        = Util.getObjectToDate(mapSubscriptions.get('date_added'));
        subscription.DataAlteracao__c               = Util.getObjectToDateTime(mapSubscriptions.get('date_last_modified'));                      
        subscription.DataCancelamento__c            = Util.getObjectToDate(mapSubscriptions.get('data_canceled'));
        subscription.Preco__c                       = Util.getDecimal(Util.getString(mapSubscriptions.get('monthly_price')));
        subscription.TipoServico__c                 = Util.getString(mapSubscriptions.get('is_lifelong')) == 'YES' ? 'Vitalício' : Util.getString(mapSubscriptions.get('service_type')) == 'servicos-premium' ? 'Premium' : Util.getString(mapSubscriptions.get('service_type')) == 'servicos_essenciais' ? 'Essencial' : '' ;
        subscription.DataLimiteReceberConteudo__c   = Util.getObjectToDate(mapSubscriptions.get('date_limit'));
        subscription.DataPagamento__c               = Util.getObjectToDate(mapSubscriptions.get('date_added'));
        subscription.DataRemocaoRecorrencia__c      = Util.getObjectToDate(mapSubscriptions.get('date_removed_recurrence'));
        subscription.PlanoID__c                     = Util.getString(mapSubscriptions.get('plan_id'));
        subscription.NomePlano__c                   = Util.getString(mapSubscriptions.get('plan'));
        subscription.PlanoSlug__c                   = Util.getString(mapSubscriptions.get('plan_slug'));
        subscription.Name                           = Util.getString(mapSubscriptions.get('id'));

        Map<String, Object> mapPayments             = ( Map<String, Object> ) mapSubscriptions.get( 'payment_data' )  ;
        subscription.DataConfirmacaoPagamento__c    = Util.getObjectToDate2(mapPayments.get('date_confirmation'));
        subscription.MeioPagamento__c               = Util.getString(mapPayments.get('acquirer_name'));
        subscription.NumeroCartao__c                = Util.getString(mapPayments.get('card_number'));
        subscription.TID__c                         = Util.getString(mapPayments.get('tid'));
        subscription.Parcelas__c                    = Util.getString(mapPayments.get('installments'));

        return subscription;
    }

    /**
    *
    *
    */
    private static Assinatura__c mapOrderStore( String codigoStore, Map<String, Object> mapOrders ) {
        Assinatura__c order                         = new Assinatura__c();
        order.Tipo__c                               = 'Pedido';
        order.Cliente__r                            = new Account(CodigoMagento__c = codigoStore);
        order.DataPagamento__c                      = Util.getObjectToDateTime(mapOrders.get('date_payment'));
        order.MeioPagamento__c                      = Util.getString(mapOrders.get('payment_method'));
        order.Preco__c                              = Util.getDecimal(Util.getString(mapOrders.get('total')));
        order.CodigoAssinaturaStore__c              = Util.getString(mapOrders.get('id'));
        
        for(Object objProductsResult : ( List<Object> ) mapOrders.get( 'products' ) ){ 
            Map<String, Object> mapProducts = ( Map<String, Object> ) objProductsResult;   
            order.Preco__c                          = Util.getDecimal(Util.getString(mapProducts.get('price')));
            order.PlanoID__c                        = Util.getString(mapProducts.get('id'));
            order.NomePlano__c                      = Util.getString(mapProducts.get('name'));
            order.Name                              = Util.getString(mapProducts.get('id'));
        }

        return order;
    }

    /**
    *   Retorna o mapa de tipo de registros criados
    *   @param mapResult mapa de dados para processamento
    */
    private static Map<String, String> mapRecordType( Map<String, Object> mapResult ) {
        List<String> tipoList = new List<String>();
        Map<String, String> mapRecordType = new Map<String, String>();

        for( Object objResult : ( List<Object> ) mapResult.get( 'account' ) ) {
            Map<String, Object> mapAccount = ( Map<String, Object> ) objResult;
            tipoList.add( Util.getString( mapAccount.get( 'tiporegistro' ) ) );
        }

        for( RecordType recordType : RecordTypeDAO.getInstance().getRecordTypeByName( tipoList, 'Account' ) )
            mapRecordType.put( recordType.Name, recordType.Id );

        return mapRecordType;    
    }

    /**
    *   Retorna modelo dados apos integração
    *   @param dmlResult resultado de inserção na base de dados
    */
    private static String response( Database.SaveResult[] dmlResult ) {
        Map<String, String> mapSObject = new Map<String, String>();
        List<String> idList = new List<String>();
        for( Database.SaveResult result : dmlResult )
            idList.add( Util.getString( result.getId() ) );

        for( Account account : AccountDAO.getInstance().getAccountsById( idList ) )
            mapSObject.put( account.Id, account.CodigoMagento__c );

        return Util.jsonResponse( dmlResult, mapSObject );
    }
}