/**
*   @author Diego Moreira
*   @class Classe DAO do objeto conta
*/
public with sharing class AccountDAO {
	/*
        Singleton
    */  
    private static final AccountDAO instance = new AccountDAO();    
    private AccountDAO(){}
    
    public static AccountDAO getInstance() {
        return instance;
    }

    /**
    *   Consulta os dados da conta pelo Id
    *   @param accountIds Lista com os ids da conta para consulta
    */
    public List<Account> getAccountsById( List<String> accountIds ) {
        return [SELECT Id, Name, FirstName, LastName, CodigoMagento__c, RazaoSocial__c, TradingName__c, NomeResponsavel__c, CPF__c, DataNascimento__c, Celular__c, 
                Email__c, InscricaoEstadual__c, CNPJ__c, Phone, DesejaReceberNewsletter__c, DesejaReceberSMS__c, Servico__c, Type, RecordType.DeveloperName                
                FROM Account
                WHERE Id in :accountIds];
    }


    /**
    *   Consulta os dados da conta pelo Id
    *   @param accountIds Lista com os ids da conta para consulta
    */
    public List<Account> getAccountsByEmails( List<String> emails ) {
        return [SELECT Id, Name, FirstName, LastName, CodigoMagento__c, RazaoSocial__c, TradingName__c, NomeResponsavel__c, CPF__c, DataNascimento__c, Celular__c, 
                Email__c, InscricaoEstadual__c, CNPJ__c, Phone, DesejaReceberNewsletter__c, DesejaReceberSMS__c, Servico__c, Type, PersonEmail                
                FROM Account
                WHERE PersonEmail in :emails];
    }
}