/**
*   @author Rogério Oliveira
*   @class Classe DAO do objeto conta
*/
public with sharing class ContactDAO {
	/*
        Singleton
    */  
    private static final ContactDAO instance = new ContactDAO();    
    private ContactDAO(){}
    
    public static ContactDAO getInstance() {
        return instance;
    }

    /**
    *   Consulta os dados do contato pelo Email
    *   @param emails Lista com os emails do contato para consulta
    */
    public List<Contact> getContactsByEmails( List<String> emails ) {
        return [SELECT Id, AccountId, Email               
                FROM Contact
                WHERE Email in :emails];
    }
}