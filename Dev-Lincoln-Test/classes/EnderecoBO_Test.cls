@isTest
private class EnderecoBO_Test {
	
	@isTest static void test_method_one() {
		Account accountInsertMagento = SObjectInstanceToClassCoverage.accountInstance( null );
		accountInsertMagento.CodigoMagento__c = '112233';
		insert accountInsertMagento;

		Endereco__c enderecoInsertMagento1 = SObjectInstanceToClassCoverage.enderecoInstance( null, accountInsertMagento.Id );
		insert enderecoInsertMagento1;

		Endereco__c enderecoInsertMagento2 = SObjectInstanceToClassCoverage.enderecoInstance( null, accountInsertMagento.Id );
		insert enderecoInsertMagento2;

		enderecoInsertMagento1.PadraoCobranca__c = true;
		update enderecoInsertMagento1;
		enderecoInsertMagento1.PadraoEntrega__c = true;
		update enderecoInsertMagento1;
	}
	
	@isTest static void test_method_two() {
		Account accountInsertMagento = SObjectInstanceToClassCoverage.accountInstance( null );
		accountInsertMagento.CodigoMagento__c = '112233';
		insert accountInsertMagento;

		Endereco__c enderecoInsertMagento = SObjectInstanceToClassCoverage.enderecoInstance( null, accountInsertMagento.Id );
		enderecoInsertMagento.CodigoMagento__c = '112233';
		insert enderecoInsertMagento;
	}
	
}