@isTest(SeeAllData=TRUE)
private class AccountInbound_Test {
    
    @isTest static void test_method_one() {
        RestRequest req = new RestRequest();
        req.addHeader('Content-Type', 'application/json'); 
        req.requestURI = '/services/apexrest/v1/account/';
        req.httpMethod = 'POST';

        String payload = '{"account" : [{' +
            '"nome" : "Test",' +
            '"sobrenome" : "account",' +
            '"nomeresponsavel" : "Test Account",' +
            '"razaosocial" : "Account",' +
            '"cpf" : "",' +
            '"celular" : "",' +
            '"email" : "teste@teste.com.br",' +
            '"nomefantasia" : "Account",' +
            '"inscricaoestadual" : "11111111",' +
            '"cnpj" : "93776490000101",' +
            '"tipo" : "Pessoa Física",' +
            '"telefone" : "",' +
            '"newsletter" : false,' +
            '"sms" : true,' +
            '"servico" : "",' +
            '"codigomagento" : "112233",' +
            '"datanascimento" : "",' +
            '"tiporegistro" : "Jolivi"}]}';
        req.requestBody = Blob.valueof( payload ); 

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        String jsonResult = AccountInboundIS.doPost();

        Queue__c queue = [SELECT Id, Name, EventName__c, Status__c, Payload__c, IgnoredByTrigger__c, Response__c, RecordId__c, ExceptionStackTrace__c
                            FROM Queue__c WHERE EventName__c = :QueueEventNames.INSERT_ACCOUNT_MAGENTO_TO_SF.name() ORDER BY Name Desc Limit 1];

        AccountInboundBO.execute( queue.Id, queue.RecordId__c, queue.EventName__c, queue.Payload__c );                  
    }
    
    @isTest static void test_method_two() {
        Account accountInsertMagento = SObjectInstanceToClassCoverage.accountInstance( null );
        insert accountInsertMagento;

        RestRequest req = new RestRequest();
        req.addHeader('Content-Type', 'application/json'); 
        req.requestURI = '/services/apexrest/v1/account/';
        req.httpMethod = 'PUT';

        String payload = '{"account" : [{' +
            '"sfid" : "' + accountInsertMagento.Id + '",' +
            '"nome" : "Test",' +
            '"sobrenome" : "account",' +
            '"nomeresponsavel" : "Test Account",' +
            '"razaosocial" : "Account",' +
            '"cpf" : "",' +
            '"celular" : "",' +
            '"email" : "teste@teste.com.br",' +
            '"nomefantasia" : "Account",' +
            '"inscricaoestadual" : "11111111",' +
            '"cnpj" : "93776490000101",' +
            '"tipo" : "Pessoa Física",' +
            '"telefone" : "",' +
            '"newsletter" : false,' +
            '"sms" : true,' +
            '"servico" : "",' +
            '"codigomagento" : "112233",' +
            '"datanascimento" : "",' +
            '"tiporegistro" : "Jolivi"}]}';
        req.requestBody = Blob.valueof( payload ); 

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        String jsonResult = AccountInboundIS.doPut();

        Queue__c queue = [SELECT Id, Name, EventName__c, Status__c, Payload__c, IgnoredByTrigger__c, Response__c, RecordId__c, ExceptionStackTrace__c
                            FROM Queue__c WHERE EventName__c = :QueueEventNames.UPDATE_ACCOUNT_MAGENTO_TO_SF.name() ORDER BY Name Desc Limit 1];

        AccountInboundBO.execute( queue.Id, queue.RecordId__c, queue.EventName__c, queue.Payload__c );
    }


    @isTest static void test_method_three() {
        RestRequest req = new RestRequest();
        req.addHeader('Content-Type', 'application/json'); 
        req.requestURI = '/services/apexrest/v1/account/';
        req.httpMethod = 'POST';

        String payload = '{"objects":[{"date_birthday":"17/05/1979","email":"rogerio@beecloud.com.br","orders":[],"cpf_cnpj":"275.325.968-26","phone":"11-99610-4536","id":265184,"mobile":"11-99610-4536","subscriptions":[],"name":"Rogério Luiz Batista De Oliveira","subscriptions_total":0,"total_spent":0}],"meta":{"next":false,"page":1,"limit":30,"previous":false,"total_count":1}}';
        req.requestBody = Blob.valueof( payload ); 

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        String jsonResult = AccountInboundIS.doPost();

        Queue__c queue = [SELECT Id, Name, EventName__c, Status__c, Payload__c, IgnoredByTrigger__c, Response__c, RecordId__c, ExceptionStackTrace__c
                            FROM Queue__c WHERE EventName__c = :QueueEventNames.QUERY_ACCOUNT_STORE_TO_SALESFORCE.name() ORDER BY Name Desc Limit 1];

        AccountInboundBO.execute( queue.Id, queue.RecordId__c, queue.EventName__c, queue.Payload__c );                  
    } 
    
}