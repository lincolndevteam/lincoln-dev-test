@isTest
private class EnderecoInbound_Test {
    
    /*@isTest static void test_method_one() {
        Account accountInsertMagento = SObjectInstanceToClassCoverage.accountInstance( null );
        insert accountInsertMagento;

        RestRequest req = new RestRequest();
        req.addHeader('Content-Type', 'application/json'); 
        req.requestURI = '/services/apexrest/v1/address/';
        req.httpMethod = 'POST';

        String payload = '{ "address" : [{' +
        '"customerid" : "' + accountInsertMagento.Id + '",' +
        '"endrua" : "R. Joaquim Floriano",' +
        '"endbairro" : "Itaim Bibi",' +
        '"endcep" : "04534013",' +
        '"endcidade" : "São Paulo",' +
        '"endcomplemento" : "",' +
        '"endestado" : "São Paulo",' +
        '"endnumero" : "913",' +
        '"isdefaultbilling" : false,' +
        '"isdefaultshipping" : false,' +
        '"telefone" : "",' +
        '"endpais" : "Brasil",' +
        '"codigomagento" : "112233" }] }';
        req.requestBody = Blob.valueof( payload ); 

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        String jsonResult = EnderecoInboundIS.doPost();

        Queue__c queue = [SELECT Id, Name, EventName__c, Status__c, Payload__c, IgnoredByTrigger__c, Response__c, RecordId__c, ExceptionStackTrace__c
                            FROM Queue__c WHERE EventName__c = :QueueEventNames.INSERT_ADDRESS_MAGENTO_TO_SF.name() ORDER BY Name Desc Limit 1];

        EnderecoInboundBO.execute( queue.Id, queue.RecordId__c, queue.EventName__c, queue.Payload__c );
    }
    
    /@isTest static void test_method_two() {
        Account accountInsertMagento = SObjectInstanceToClassCoverage.accountInstance( null );
        accountInsertMagento.CodigoMagento__c = '112233';
        insert accountInsertMagento;

        Endereco__c enderecoInsertMagento = SObjectInstanceToClassCoverage.enderecoInstance( null, accountInsertMagento.Id );
        insert enderecoInsertMagento;

        RestRequest req = new RestRequest();
        req.addHeader('Content-Type', 'application/json'); 
        req.requestURI = '/services/apexrest/v1/address/';
        req.httpMethod = 'PUT';

        String payload = '{ "address" : [{' +
        '"sfid" : "' + enderecoInsertMagento.Id + '", ' +
        '"customerid" : "' + accountInsertMagento.Id + '",' +
        '"endrua" : "R. Joaquim Floriano",' +
        '"endbairro" : "Itaim Bibi",' +
        '"endcep" : "04534013",' +
        '"endcidade" : "São Paulo",' +
        '"endcomplemento" : "",' +
        '"endestado" : "São Paulo",' +
        '"endnumero" : "913",' +
        '"isdefaultbilling" : false,' +
        '"isdefaultshipping" : false,' +
        '"telefone" : "",' +
        '"endpais" : "Brasil",' +
        '"codigomagento" : "112233" }] }';
        req.requestBody = Blob.valueof( payload ); 

        RestResponse res = new RestResponse();
        RestContext.request = req;
        RestContext.response = res;
        String jsonResult = EnderecoInboundIS.doPut();

        Queue__c queue = [SELECT Id, Name, EventName__c, Status__c, Payload__c, IgnoredByTrigger__c, Response__c, RecordId__c, ExceptionStackTrace__c
                            FROM Queue__c WHERE EventName__c = :QueueEventNames.UPDATE_ADDRESS_MAGENTO_TO_SF.name() ORDER BY Name Desc Limit 1];

        EnderecoInboundBO.execute( queue.Id, queue.RecordId__c, queue.EventName__c, queue.Payload__c ); } */
    
}