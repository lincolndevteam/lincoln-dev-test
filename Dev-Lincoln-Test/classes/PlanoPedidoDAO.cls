public class PlanoPedidoDAO {

    
    /*
        Singleton
    */
    private static final PlanoPedidoDAO instance = new PlanoPedidoDAO();    
    private PlanoPedidoDAO(){}
    
    public static PlanoPedidoDAO getInstance() {
        return instance;
    } 
    
    public List<PlanoPedido__c> getPlanOrder( List<String> orderId ) {
        return [SELECT Id
                FROM PlanoPedido__c
                WHERE Pedido__c in :orderId];
    }
    
}