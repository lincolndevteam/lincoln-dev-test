/**
*    @author Rogério Oliveira
*    @class Classe de negocio de Conta
*/
public with sharing class EmailMessageBO {
   /*
        Singleton
    */
    private static final EmailMessageBO instance = new EmailMessageBO();    
    private EmailMessageBO(){}
    
    public static EmailMessageBO getInstance() {
        return instance;
    }

    /**
    *
    *
    */
    public void preventDeletionEmailMessage(EmailMessage [] pEmailMessage, EmailMessage [] pOldEmailMessage) {

        //EmailMessage deletion is only allowed for administrator

        String profileName=[Select Id,Name from Profile where Id=:userinfo.getProfileId()].Name;
        for(EmailMessage currentEmailMessage : pOldEmailMessage) {
            
            //Check if current user is not a system administrator
            if(profileName !='System Administrator'){
                currentEmailMessage.addError(System.Label.MsgErroExcluirMsgEmail);
            }
        }
    }

    /**
    *   Atualiza o email do yahoo
    *   @param emailist Lista com o email recebido
    */
    public void updateEmail( List<EmailMessage> emailist ) {
        for( EmailMessage emailMessage : emailist ) {
            if( emailMessage.FromAddress != null &&
                    emailMessage.FromAddress.contains( '.yahoo.' ) ) {
                emailMessage.FromAddress = Util.updateYahooMail( emailMessage.FromAddress );
            }
        }
    }

}