/**
*    @author Diego Moreira
*    @class Classe de negocio para o objeto queue
*/
public class QueueBO {
    /*
        Singleton
    */
    private static final QueueBO instance = new QueueBO();    
    private QueueBO(){}
    
    public static QueueBO getInstance() {
        return instance;
    }

    /**
    *    @enum para representação dos status da fila
    */
    public enum QueueStatus {
        CREATED,
        SUCCESS,
        ERROR
    }
    
    /* 
        Mapa de eventos e classe de execução
    */
    private static Map<String, IQueueProcessing> mapToExecute;     
    static {
        mapToExecute = new Map<String, IQueueProcessing>();
        mapToExecute.put( QueueEventNames.INSERT_ACCOUNT_MAGENTO_TO_SF.name(), new AccountInboundBO() );
        mapToExecute.put( QueueEventNames.UPDATE_ACCOUNT_MAGENTO_TO_SF.name(), new AccountInboundBO() );
        mapToExecute.put( QueueEventNames.INSERT_ADDRESS_MAGENTO_TO_SF.name(), new EnderecoInboundBO() );
        mapToExecute.put( QueueEventNames.UPDATE_ADDRESS_MAGENTO_TO_SF.name(), new EnderecoInboundBO() );
        mapToExecute.put( QueueEventNames.INSERT_ACCOUNT_SF_TO_MAGENTO.name(), new AccountOutboundBO() );
        mapToExecute.put( QueueEventNames.UPDATE_ACCOUNT_SF_TO_MAGENTO.name(), new AccountOutboundBO() );
        mapToExecute.put( QueueEventNames.INSERT_ADDRESS_SF_TO_MAGENTO.name(), new EnderecoOutboundBO() );
        mapToExecute.put( QueueEventNames.UPDATE_ADDRESS_SF_TO_MAGENTO.name(), new EnderecoOutboundBO() );
        mapToExecute.put( QueueEventNames.QUERY_ACCOUNT_STORE_TO_SALESFORCE.name(), new AccountInboundBO() );
    }
 
    /**
    *    Cria fila de processamento
    *    @param eventName string com o nome do evento a ser processado
    *    @param payload string com os JSON do objeto a ser processado
    *    @param ignoredByTrigger boolean para avisar se a fila vai ser executada pela trigger ou outro processo
    */ 
    public String insertQueue( String eventName, String payLoad, Boolean ignoredByTrigger ) {
        Queue__c queue              = new Queue__c();
        queue.EventName__c          = eventName;
        queue.Payload__c            = payLoad;
        queue.Status__c             = QueueStatus.CREATED.name();
        queue.IgnoredByTrigger__c   = ignoredByTrigger;
        
        insert queue; 
        return queue.Id;
    }

    /**
    *    Monta objeto de fila fila de processamento
    *    @param eventName string com o nome do evento a ser processado
    *    @param payload string com os JSON do objeto a ser processado
    *    @param ignoredByTrigger boolean para avisar se a fila vai ser executada pela trigger ou outro processo
    */ 
    public Queue__c sObjectQueue( String recordId, String eventName, String payLoad, Boolean ignoredByTrigger ) {
        Queue__c queue              = new Queue__c();
        queue.RecordId__c           = recordId;
        queue.EventName__c          = eventName;
        queue.Payload__c            = payLoad;
        queue.Status__c             = QueueStatus.CREATED.name();
        queue.IgnoredByTrigger__c   = ignoredByTrigger;
         
        return queue;
    }
    
    /**
    *    Atualiza fila de processamento
    *    @param queueId string com o id da fila para atualização
    *    @param dmlExceptionStackTrace string com possivel erro de processamento da fila
    */
    public void updateQueue( String queueId, String dmlExceptionStackTrace ) {
        Queue__c queue                  = new Queue__c();
        queue.Id                        = queueId;
        queue.Status__c                 = dmlExceptionStackTrace.equals('') ? QueueStatus.SUCCESS.name() : QueueStatus.ERROR.name();
        queue.ExceptionStackTrace__c    = dmlExceptionStackTrace;       
        
        update queue;
    }

    /**
    *    Atualiza fila de processamento
    *    @param queueId string com o id da fila para atualização
    *    @param dmlExceptionStackTrace string com possivel erro de processamento da fila
    */
    public void updateQueue( String queueId, Database.UpsertResult[] upsertResult ) {
        

        Queue__c queue                  = new Queue__c();
        queue.Id                        = queueId;
        
        queue.Status__c                 = upsertResult.size() == 0 ? QueueStatus.ERROR.name() : upsertResult[0].isSuccess() ? QueueStatus.SUCCESS.name() : QueueStatus.ERROR.name();
        //Database.Error err              = !upsertResult[0].isSuccess() ? upsertResult.getErros()[0] : '';
        //queue.ExceptionStackTrace__c    = err.getMessage();     
        
        update queue;
    }

    /**
    *   Atualiza fila de processamento
    *   @param queueId string com o id da fila para atualização
    *   @param response Retorno do serviço gerado    
    *   @param dmlExceptionStackTrace string com possivel erro de processamento da fila
    */
    public void updateQueue( String queueId, String response, String dmlExceptionStackTrace ) {
        Queue__c queue                  = new Queue__c();
        queue.Id                        = queueId;     
        queue.Status__c                 = dmlExceptionStackTrace.equals('') ? QueueStatus.SUCCESS.name() : QueueStatus.ERROR.name();
        queue.Response__c               = response;
        queue.ExceptionStackTrace__c    = dmlExceptionStackTrace;       
        
        update queue;
    }
    
    /*
        Repocessa os dados da fila
    */ 
    public void reprocessQueue() {
        List<Queue__c> queuesResult = QueueDAO.getInstance().getQueueByStatus( new List<String>{ QueueStatus.CREATED.name() } );
        execute( queuesResult );
    }    

    /**
    *    Executa o processamento da fila
    *    @param queueToProcessing filas para processamento
    */
    public void execute( List<Queue__c> queueToProcessing ) {
        for( Queue__c queue : queueToProcessing ) {
            if( Test.isRunningTest() ) return;
            if( queue.IgnoredByTrigger__c ) return;

            mapToExecute.get( queue.EventName__c ).execute( queue.Id, queue.RecordId__c, queue.EventName__c, queue.Payload__c );   
        }
    }
}