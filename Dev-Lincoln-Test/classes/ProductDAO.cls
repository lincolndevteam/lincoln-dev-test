public with sharing class ProductDAO {

	/*
        Singleton
    */
    private static final ProductDAO instance = new ProductDAO();    
    private ProductDAO(){}
    
    public static ProductDAO getInstance() {
        return instance;
    } 
    
    public String getClassificacaoProduto( String storedCode ) {
        List<Product2> product = [SELECT Classificacao__c
                FROM Product2
                WHERE CodigoStored__c = :storedCode
                Limit 1];
        return product[0].Classificacao__c;
    }

    public List<Product2> getProductByStoredId( String storedCode ) {
        List<Product2> product = [SELECT Classificacao__c, Conteudo__c
                FROM Product2
                WHERE CodigoStored__c = :storedCode
                Limit 1];
        return product;
    }

}