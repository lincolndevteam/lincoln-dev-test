public class DescontoOfertaDAO {
    
        /*
        Singleton
    */
    private static final DescontoOfertaDAO instance = new DescontoOfertaDAO();    
    private DescontoOfertaDAO(){}
    
    public static DescontoOfertaDAO getInstance() {
        return instance; 
    }
    
    public List<DescontoOferta__c> getDescontoOfertaByPlanoOfertaId( String planoOfertaId ) {
        return [SELECT Id, Name, Duracao__c, PlanoOferta__c
                FROM DescontoOferta__c
                WHERE PlanoOferta__c = :planoOfertaId];
    }

}