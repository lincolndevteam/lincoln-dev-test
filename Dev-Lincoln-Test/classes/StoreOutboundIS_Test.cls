@isTest(SeeAllData=TRUE)
private class StoreOutboundIS_Test {
     @isTest static void testCallout() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        StoreOutboundIS.getInstance().getStoreCustomerByEmail('diego@beecloud.com.br');
        StoreOutboundIS.getInstance().getStoreSubscriptionsByCustomerStoreId('9999999');

        Account thisAccount = SObjectInstanceToClassCoverage.accountEmpiricusInstance( NULL );
		insert thisAccount;

		Case thisCase = SObjectInstanceToClassCoverage.casewithcontactInstance( thisAccount.Id );
		insert thisCase;	
    }  
}