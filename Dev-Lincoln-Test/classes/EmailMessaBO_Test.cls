@isTest(SeeAllData=TRUE)
public with sharing class EmailMessaBO_Test {
    public EmailMessaBO_Test() {
    }

    @isTest static void deleteEmailMessageWithAdminUserTest(){

      Profile p = [SELECT Id FROM Profile WHERE Name='Administrador do sistema'];
      User u=new User(Alias='roliv',Email='20170121@test.com',
      EmailEncodingKey='UTF-8',LastName='Testing',LanguageLocaleKey='en_US',
      LocaleSidKey='en_US',ProfileId = p.Id,
      TimeZoneSidKey='America/Los_Angeles',UserName='20170121@test.com');
      insert u;

      System.runAs(u){
        Account tmpAccount = SObjectInstanceToClassCoverage.accountEmpiricusInstance(NULL);
        insert tmpAccount;

      Case tmpCase=new Case();
        tmpCase.Status='Aberto';
        tmpCase.Origin='Telefone';
        tmpCase.Type='Relacionamento';
        tmpCase.AccountId=tmpAccount.id;
        tmpCase.Respondido__c = false;
        tmpCase.SuppliedEmail='folhacachoeirinha.yahoo.com.br@mail1.empiricus.com.br';
        tmpCase.SuppliedName='folhacachoeirinha.yahoo.com.br@mail1.empiricus.com.br';
        
      insert tmpCase;

        EmailMessage tmpEmailMessage = new EmailMessage();
        tmpEmailMessage.ParentID=tmpCase.id;
        tmpEmailMessage.Status='0';
        tmpEmailMessage.InComing=true;        
        tmpEmailMessage.Subject='test';
        tmpEmailMessage.TextBody='Test';
        tmpEmailMessage.FromAddress='folhacachoeirinha.yahoo.com.br@mail1.empiricus.com.br';
        tmpEmailMessage.FromName='walter herman folhacachoeirinhayahoo.com.br';
        tmpEmailMessage.ToAddress='teste.atendimento@empiricus.com.br';

        insert tmpEmailMessage;

        Database.DeleteResult result=Database.delete(tmpEmailMessage,true);
        System.assert(result.isSuccess());
        System.assert(result.getErrors().size()==0);
        }
        
        SObjectInstanceToClassCoverage testing = new SObjectInstanceToClassCoverage();       
        testing.fcoverage();

      }
   // }
    
    @isTest static void deleteEmailMessageWithUserTest(){
      //A non administrator user should not be able to delete an Email Message
      Profile p=[SELECT Id FROM Profile WHERE Name='Empiricus - Agente de Atendimento'];
      User u=new User(Alias='standt',Email='20170120@test.com',
      EmailEncodingKey='UTF-8',LastName='Testing',LanguageLocaleKey='en_US',
      LocaleSidKey='en_US', ProfileId=p.Id,
      TimeZoneSidKey='America/Los_Angeles', UserName='20170120@test.com');
      insert u;

      System.runAs(u){
        Account tmpAccount = SObjectInstanceToClassCoverage.accountEmpiricusInstance(NULL);
        insert tmpAccount;

        Case tmpCase=new Case();
        tmpCase.Status='Low';
        tmpCase.Origin='Phone';       
        tmpCase.AccountId=tmpAccount.id;
        insert tmpCase;
        
        EmailMessage tmpEmailMessage=new EmailMessage();
        tmpEmailMessage.ParentID=tmpCase.id;
        tmpEmailMessage.TextBody='Test';
        insert tmpEmailMessage;

        Database.DeleteResult result=Database.delete(tmpEmailMessage,false);
        System.assert(!result.isSuccess());
        System.assert(result.getErrors().size()>0);
        }
    }
}