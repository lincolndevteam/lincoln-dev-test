public class OfertaDAO {

    /*
        Singleton
    */
    private static final OfertaDAO instance = new OfertaDAO();    
    private OfertaDAO(){}
    
    public static OfertaDAO getInstance() {
        return instance;
    } 
    
    public List<Oferta__c> getOffer(Set<String> offerList ) {
        return [SELECT Id, Name, TipoDesconto__c, Desconto__c
                FROM Oferta__c
                WHERE Id in :offerList];
    }
}