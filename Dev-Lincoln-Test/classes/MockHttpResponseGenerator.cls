@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('https://store.empiricus.com.br/api/crm/orders/?email=addressEmail&token=fc9a6372-0d87-46b0-8b70-82925674a427', req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"objects":[{"date_birthday":"17/05/1979","email":"rogerio@beecloud.com.br","orders":[],"cpf_cnpj":"275.325.968-26","phone":"11-99610-4536","id":265184,"mobile":"11-99610-4536","subscriptions":[],"name":"Rogério Luiz Batista De Oliveira","subscriptions_total":0,"total_spent":0}],"meta":{"next":false,"page":1,"limit":30,"previous":false,"total_count":1}}');
        res.setStatusCode(200);
        return res;
    }
}