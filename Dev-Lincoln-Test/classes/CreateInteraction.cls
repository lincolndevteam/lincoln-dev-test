public with sharing class CreateInteraction {

    public String division{get;set;}
    public SelectOption[] selectedUsers { get; set; }
    public SelectOption[] allUsers { get; set; }
    
    public String message { get; set; }

    public CreateInteraction(ApexPages.StandardController controller) {

        selectedUsers = new List<SelectOption>();
        
        List<User> users = [SELECT Name, Id FROM User];    
        allUsers = new List<SelectOption>();
        for ( User c : users ) {
            allUsers.add(new SelectOption(c.Id, c.Name));
        }
    }

}