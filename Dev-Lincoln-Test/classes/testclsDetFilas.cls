@istest
public class testclsDetFilas {
    static testMethod void testNewQueue() {
        PageReference pageRef = Page.DetFilas;
        test.setCurrentPage(pageRef);
        Group gp = new Group();
        gp.DeveloperName='teste';
        gp.DoesSendEmailToMembers=false;
        gp.Name='teste';
        ApexPages.StandardController scq = new ApexPages.StandardController(gp);
        clsDetfilas cls = new clsDetfilas(scq);
       PageReference pg = cls.atualizaPagina();
        List<SelectOption> objl = cls.getsObjects();
        PageReference pgVolta=cls.Voltar();
        List<SelectOption> obja =cls.getTiposDisp();
        cls.Tipo='U';
        List<SelectOption> obju =cls.getUserOrGroup();
        cls.Tipo='G';
        List<SelectOption> objg =cls.getUserOrGroup();
        //cls.fila=gp;
        String usrstr = system.UserInfo.getUserId();
        List<string> lstobj = new List<String>();
        lstobj.add('Case');
        List<String> lstUser = new List<String>();
        lstuser.add(usrstr);
        cls.Objetos = lstobj;
        cls.UsersOrGroups=lstUser;
        cls.SalvarFila();
        cls.ExcluirObjeto();
    }
    
}